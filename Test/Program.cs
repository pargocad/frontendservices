﻿using System;
using System.Collections.Generic;
using System.IO;
using br.ufc.pargo.hpe.backend.DGAC;
using br.ufc.pargo.hpe.backend.DGAC.database;

namespace Test
{

    class MainClass
    {
//        private static string FILE_NAME_1 = null;
//		private static string FILE_NAME_2 = null;

		public static void Main(string[] args)
        {
            Console.WriteLine(args[0]);

      //      FILE_NAME_1 = args[0];
      //      FILE_NAME_2 = args[1]; 

      //      string contents_1 = File.ReadAllText(FILE_NAME_1);
		//	string contents_2 = File.ReadAllText(FILE_NAME_2);

//            Instantiator.ComponentFunctorApplicationType contextual_type_1 = LoaderApp.deserialize<Instantiator.ComponentFunctorApplicationType>(contents_1);
//			Instantiator.ComponentFunctorApplicationType contextual_type_2 = LoaderApp.deserialize<Instantiator.ComponentFunctorApplicationType>(contents_2);

            BackEnd.ResolveComponentHierarchy r = BackEnd.resolveComponent("mapreduce.gust.iterative.sssp", "shuffler");

			BackEnd.ConverterComponentHierachyToXML s = new BackEnd.ConverterComponentHierachyToXML();

			XMLComponentHierarchy.ComponentHierarchyType ctree_xml = (XMLComponentHierarchy.ComponentHierarchyType)s.visit(r);
            string ctree_xml_str = LoaderApp.serialize<XMLComponentHierarchy.ComponentHierarchyType>(ctree_xml);

            File.WriteAllText("/home/heron/shuffler.xml", ctree_xml_str);


			/*    IDictionary<string, IList<BackEnd.ResolveComponentHierarchy>> m = new Dictionary<string, IList<BackEnd.ResolveComponentHierarchy>>();
				IList<BackEnd.ResolveComponentHierarchy> l = new List<BackEnd.ResolveComponentHierarchy>();
				l.Add(r_inner);
				m["test." + r_inner.ID] = l;
				BackEnd.ResolveComponentHierarchy r = BackEnd.resolveComponent("test", contextual_type_1, m);
				(new ShowChoices()).visit(r);

				BackEnd.ConverterComponentHierachyToXML s = new BackEnd.ConverterComponentHierachyToXML();

				XMLComponentHierarchy.ComponentHierarchyType ctree_xml = (XMLComponentHierarchy.ComponentHierarchyType)s.visit(r);

				string ctree_xml_str = LoaderApp.serialize<XMLComponentHierarchy.ComponentHierarchyType>(ctree_xml);
				Console.WriteLine(ctree_xml_str);

				Console.WriteLine("OTHER: ");*/

			//BackEnd.ComponentHierarchy r_prime = BackEnd.convertXMLToComponentHierarchy(ctree_xml);
			//(new ShowChoices()).visit(r_prime);


			/*BackEnd.DependenciesCalculator dependencies_calculator = new BackEnd.DependenciesCalculator();
            dependencies_calculator.visit(r);

            Console.WriteLine("DEPENDENCIES: ");

            List<KeyValuePair<string, int>> myList = new List<KeyValuePair<string, int>>(dependencies_calculator.Dependencies);

			myList.Sort(
				delegate (KeyValuePair<string, int> pair1,
				KeyValuePair<string, int> pair2)
				{
					return pair1.Value.CompareTo(pair2.Value);
				}
			);

            foreach (KeyValuePair<string, int> d in myList)
			  Console.WriteLine("{0}:{1}", d.Key, d.Value);
            */

			return;
        }

        public class ShowChoices : BackEnd.ResolveComponentHierarchyVisitor
        {
            int level = 0;

            public object visit(BackEnd.ResolveComponentHierarchy ctree)
            {
                Console.WriteLine("{0} -- {1}", ctree.ComponentChoice, ctree.GetHashCode());

                foreach (KeyValuePair<string, IList<BackEnd.ResolveComponentHierarchy>> n in ctree.InnerComponents)
                {
                    if (n.Value.Count > 1) Console.WriteLine("< {0} ****", n.Key);
                    foreach (BackEnd.ResolveComponentHierarchy nn in n.Value)
                    {
                        level++;
                        for (int i = 0; i < level; i++) Console.Write("|");
						Console.Write( " SON of -- {0} / ", n.Key);
						visit(nn);
                        level--;
                    }
                    if (n.Value.Count > 1) Console.WriteLine("**** {0} >", n.Key);
                }

                return null;
            }
        }

    }
}
