#include <stdlib.h>
#include <stdio.h>
#include "mpi.h"

struct ConnectorType
{
	int(*send)(const void*, int, MPI_Datatype, int, int, int);
	int(*receive)(void*, int*, MPI_Datatype, int, int, int, MPI_Status*);
	int facet;
	int facet_count;
	int *facet_size;
	int *facet_instance;
};

struct ConnectorType* connector = NULL;

int registerChannel(int n, 
		            int connector_id, 
					int facet,
					int facet_count,
					int* facet_instance,
					int* facet_size,
					int(*send)(const void*, int, MPI_Datatype, int, int, int), 
					int(*receive)(void*, int*, MPI_Datatype, int, int, int, MPI_Status*))
{
    if (!connector)
        connector = (struct ConnectorType*) malloc(n*sizeof(struct ConnectorType));

    if (connector_id < 0 || connector_id >=n)
       return MPI_ERR_ARG;

    printf("REGISTER CONNECTOR (mpirun) send=%p / receive=%p / facet=%d, facet_count=%d **** ", send, receive, facet, facet_count);
    
    connector[connector_id].facet = facet;
    connector[connector_id].facet_count = facet_count;
    connector[connector_id].facet_instance = facet_instance;
    connector[connector_id].facet_size = facet_size;
    connector[connector_id].send = send;
    connector[connector_id].receive = receive;

    return MPI_SUCCESS;
}

void run(MPI_Comm comm)
{
  int rank;

  MPI_Comm_rank(comm, &rank);
  
  int facet = connector[0].facet;
  int facet_count = connector[0].facet_count;
  int* facet_instance = connector[0].facet_instance;
  int* facet_size = connector[0].facet_size;

  int next_facet_ = facet == 3 ? 0 : facet + 1; 
  int prev_facet_ = facet == 0 ? 3 : facet - 1; 
  
  int this_facet = *(facet_instance + facet);
  int next_facet = *(facet_instance + next_facet_);
  int prev_facet = *(facet_instance + prev_facet_);

  printf(">>>>>>>> RANK = %d, FACET = %d, THIS_FACET=%d, NEXT_FACET=%d, PREV_FACET=%d *************   ", rank, facet, this_facet, next_facet, prev_facet);
  
  int value = this_facet;
  for (int i=0; i<facet_count; i++)
  {
	  printf("BEGIN process %d at facet %d sending %d to %d at facet %d *** ", rank, this_facet, value, rank, next_facet);
	  connector[0].send(&value, 1, MPI_INT, next_facet, rank, 0);  
	  printf("END process %d at facet %d sending %d to %d at facet %d *** ", rank, this_facet, value, rank, next_facet);
	  printf("process %d at facet %d receiving from %d at facet %d *** ", rank, this_facet, rank, prev_facet);
	  MPI_Status status;
	  int count;
	  int new_value;
	  connector[0].receive(&new_value, &count, MPI_INT, prev_facet, rank, 0, &status);
	  printf("process %d at facet %d received %d from %d at facet %d *** count=%d", rank, this_facet, new_value, rank, prev_facet, count);
	  value = new_value;
  }
}

