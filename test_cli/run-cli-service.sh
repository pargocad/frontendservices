#!/bin/sh

export CORE_ADDRESS=127.0.0.1
export CORE_PORT=8080
export PlatformSAFe_ADDRESS=127.0.0.1
export PlatformSAFe_PORT=8079
export TEMPLATE_CODE_CONCRETE="$HPCShelf_HOME/Case Study - Alite/workspace/br.ufc.mdcc.hpcshelf.mpi.impl.MPILauncherForCImpl/MPILauncherForCImpl.hpe"
export TEMPLATE_CODE_ABSTRACT="$HPCShelf_HOME/Case Study - Alite/workspace/br.ufc.mdcc.hpcshelf.mpi.MPILauncher/MPILauncher.hpe"

#cd $HPCShelf_HOME/FrontendServices/HPCShelfCLI/bin/Debug

chmod +x $HPCShelf_HOME/FrontendServices/HPCShelfCLI/bin/Debug/hpc_shelf_service.exe
rm hpc_shelf_service.lock
mono-service $HPCShelf_HOME/FrontendServices/HPCShelfCLI/bin/Debug/hpc_shelf_service.exe -d:./ -l:hpc_shelf_service.lock --debug --no-daemon

#cd $HPCShelf_HOME
