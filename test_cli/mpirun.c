#include <stdio.h>
#include "mpi.h"
#include <stdlib.h>

void run(MPI_Comm comm)
{
  int rank;
  FILE *fptr;

  fptr = fopen("output_mpi.txt","w");

  MPI_Comm_rank(comm, &rank);

  fprintf(fptr, "MY RANK IS %d", rank);

  fclose(fptr);
}

