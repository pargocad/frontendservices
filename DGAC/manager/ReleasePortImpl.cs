﻿using gov.cca;
using org.hpcshelf.DGAC;
using System.Collections.Generic;
using org.hpcshelf.ports;

namespace org.hpcshelf.ports
{
    public class MultipleReleasePortImpl : MultipleReleasePort
    {
        private IList<ReleasePort> ports = new List<ReleasePort>();

        public void addPort(ReleasePort port)
        {
            ports.Add(port);
        }

        public void release1()
        {
            foreach (ReleasePort port in ports)
                port.release1();
        }

        public void release2()
        {
            foreach (ReleasePort port in ports)
                port.release2();
        }
    }

    public class ReleasePortImpl : ReleasePort
    {
        private ManagerComponentID mcid = null;
        private string session_id_string = null;
        private Port port_worker = null;

        public ReleasePortImpl(ManagerServices services, Port port_worker)
        {
            this.mcid = (ManagerComponentID)services.getComponentID();
            this.session_id_string = mcid.getInstanceName();
            this.port_worker = port_worker;
        }

        #region AutomaticSlicesPort implementation
        public void release1()
        {
            var port_worker_release = (ReleasePort)port_worker;
            port_worker_release.release1();
        }

        public void release2()
        {
            var port_worker_release = (ReleasePort)port_worker;
            port_worker_release.release2();
        }

        #endregion
    }
}

