﻿﻿﻿﻿using System;
using System.Collections.Generic;
using System.Threading;
using org.hpcshelf.DGAC.utils;
using XMLComponentHierarchy;
using static org.hpcshelf.DGAC.BackEnd;

namespace org.hpcshelf.DGAC
{
    public class ConverterComponentHierachyToXML : ResolveComponentHierarchyVisitor
    {
        public object visit(ResolveComponentHierarchy ctree)
        {
            XMLComponentHierarchy.ComponentHierarchyType ctree_xml = new XMLComponentHierarchy.ComponentHierarchyType();

            ctree_xml.component_choice = ctree.ComponentChoice[0];
            if (ctree.ComponentChoice.Length > 1)
                ctree_xml.component_choice_2 = ctree.ComponentChoice[1];

            ctree_xml.id = ctree.ID;

            ctree_xml.kind = Constants.kindString[ctree.Kind];

            ctree_xml.is_qualifierSpecified = false;

            ctree_xml.host_component = new XMLComponentHierarchy.StringListType[ctree.HostComponents.Count];

            // HOST COMPONENTS

            int i = 0;
            foreach (Tuple<string, ResolveComponentHierarchy> ctree_host_list in ctree.HostComponents)
            {
                ctree_xml.host_component[i] = new XMLComponentHierarchy.StringListType();
                ctree_xml.host_component[i].id_inner = ctree_host_list.Item1;
                ctree_xml.host_component[i].id = new string[1];
                ctree_xml.host_component[i].id[0] = ctree_host_list.Item2.ID;
                i++;
            }

            // INNER COMPONENTS

            IList<XMLComponentHierarchy.PrivateInnerComponentType> private_inner_component_list = new List<XMLComponentHierarchy.PrivateInnerComponentType>();
            IList<XMLComponentHierarchy.PublicInnerComponentType> public_inner_component_list = new List<XMLComponentHierarchy.PublicInnerComponentType>();

            foreach (KeyValuePair<string, IList<Tuple<ResolveComponentHierarchy, int[]>>> ctree_inner_pair in ctree.InnerComponents)
            {
                string id_inner = ctree_inner_pair.Key;
                IList<Tuple<ResolveComponentHierarchy, int[]>> ctree_inner_list = ctree_inner_pair.Value;

                if (ctree_inner_list.Count == 0)
                    continue;

                ResolveComponentHierarchy ctree_inner = ctree_inner_list[0].Item1;
                int[] ctree_inner_facets = ctree_inner_list[0].Item2 != null ? ctree_inner_list[0].Item2 : new int[0];

                if (ctree_inner.Kind == Constants.KIND_QUALIFIER)
                    continue;

                if (ctree.IsPrivate[id_inner])
                {
                    XMLComponentHierarchy.ComponentHierarchyType ctree_inner_xml_0 = (XMLComponentHierarchy.ComponentHierarchyType)visit(ctree_inner);
                    XMLComponentHierarchy.PrivateInnerComponentType ctree_inner_xml_1 = new XMLComponentHierarchy.PrivateInnerComponentType();
                    ctree_inner_xml_1.id_inner = id_inner;
                    ctree_inner_xml_1.kind = ctree_inner_xml_0.kind;
                    ctree_inner_xml_1.component_choice = ctree_inner_xml_0.component_choice;
					ctree_inner_xml_1.component_choice_2 = ctree_inner_xml_0.component_choice_2;
					ctree_inner_xml_1.id = ctree_inner_xml_0.id;
                    ctree_inner_xml_1.is_qualifierSpecified = ctree_inner_xml_0.is_qualifierSpecified;
                    ctree_inner_xml_1.is_qualifier = ctree_inner_xml_0.is_qualifier;
                    ctree_inner_xml_1.private_inner_component = ctree_inner_xml_0.private_inner_component;
                    ctree_inner_xml_1.public_inner_component = ctree_inner_xml_0.public_inner_component;
                    ctree_inner_xml_1.host_component = ctree_inner_xml_0.host_component;

                    ctree_inner_xml_1.unit = ctree_inner_xml_0.unit;
					ctree_inner_xml_1.unit_2 = ctree_inner_xml_0.unit_2;
					ctree_inner_xml_1.facet_list = ctree_inner_xml_0.facet_list;

                    ctree_inner_xml_1.facet = ctree_inner_facets;

                    private_inner_component_list.Add(ctree_inner_xml_1);

                }
                else
                {
                    XMLComponentHierarchy.PublicInnerComponentType ctree_inner_xml = new XMLComponentHierarchy.PublicInnerComponentType();
                    public_inner_component_list.Add(ctree_inner_xml);

                    ctree_inner_xml.id_inner = id_inner;

                    ctree_inner_xml.inner = new PublicInnerComponentIdType[ctree_inner_list.Count];

                    int j = 0;
                    foreach (Tuple<ResolveComponentHierarchy, int[]> ctree_inner_item in ctree_inner_list)
                    {
                        ctree_inner_xml.inner[j] = new PublicInnerComponentIdType();
                        ctree_inner_xml.inner[j].id = ctree_inner_item.Item1.ID;
                        ctree_inner_xml.inner[j].facet = ctree_inner_item.Item2;
                        j++;
                    }
                }

            }

            // UNIT INFO

            IList<Instantiator.UnitMappingType> unit_list = new List<Instantiator.UnitMappingType>();

            {
                IDictionary<string, IList<Tuple<int, int[], int, string, string, string[]>>> unit_mapping_dict;
                BackEnd.copy_unit_mapping_to_dictionary(ctree.UnitMapping[0], out unit_mapping_dict);

                foreach (KeyValuePair<string, IList<Tuple<int, int[], int, string, string, string[]>>> unit_mapping_info_list in unit_mapping_dict)
                {
                    string id_interface = unit_mapping_info_list.Key;
                    foreach (Tuple<int, int[], int, string, string, string[]> unit_mapping_info_item in unit_mapping_info_list.Value)
                    {
                        Instantiator.UnitMappingType ux = new Instantiator.UnitMappingType();

                        ux.assembly_string = unit_mapping_info_item.Item4;
                        ux.class_name = unit_mapping_info_item.Item5;
                        ux.unit_id = id_interface;
                        ux.unit_index = 0;
                        ux.port_name = unit_mapping_info_item.Item6;

                        ux.facet_instanceSpecified = true;
                        ux.facet_instance = unit_mapping_info_item.Item1;

                        ux.facetSpecified = true;
                        ux.facet = unit_mapping_info_item.Item3;

                        ux.node = unit_mapping_info_item.Item2;
                        unit_list.Add(ux);
                    }
                }
            }

			IList<Instantiator.UnitMappingType> unit_list_2 = new List<Instantiator.UnitMappingType>();			
            if (ctree.UnitMapping.Length > 1)
            {

				IDictionary<string, IList<Tuple<int, int[], int, string, string, string[]>>> unit_mapping_dict;
				BackEnd.copy_unit_mapping_to_dictionary(ctree.UnitMapping[1], out unit_mapping_dict);

				foreach (KeyValuePair<string, IList<Tuple<int, int[], int, string, string, string[]>>> unit_mapping_info_list in unit_mapping_dict)
				{
					string id_interface = unit_mapping_info_list.Key;
					foreach (Tuple<int, int[], int, string, string, string[]> unit_mapping_info_item in unit_mapping_info_list.Value)
					{
						Instantiator.UnitMappingType ux = new Instantiator.UnitMappingType();

						ux.assembly_string = unit_mapping_info_item.Item4;
						ux.class_name = unit_mapping_info_item.Item5;
						ux.unit_id = id_interface;
						ux.unit_index = 0;
						ux.port_name = unit_mapping_info_item.Item6;

						ux.facet_instanceSpecified = true;
						ux.facet_instance = unit_mapping_info_item.Item1;

						ux.facetSpecified = true;
						ux.facet = unit_mapping_info_item.Item3;

						ux.node = unit_mapping_info_item.Item2;
						unit_list_2.Add(ux);
					}
				}

			}


            // FACET INFO

            IList<XMLComponentHierarchy.FacetType> facet_list = new List<XMLComponentHierarchy.FacetType>();

            for (int index = 0; index < ctree.FacetList.Length; index++)
            {
                //if (ctree.FacetList[index].Count > 0)
                //{
                    XMLComponentHierarchy.FacetType fx = new XMLComponentHierarchy.FacetType();

                    fx.facet_instanceSpecified = true;
                    fx.facet_instance = index;

                    fx.facet = new int[ctree.FacetList[index].Count];
                    ctree.FacetList[index].CopyTo(fx.facet, 0);

                    facet_list.Add(fx);
                //}
            }

            if (private_inner_component_list.Count > 0)
            {
                ctree_xml.private_inner_component = new XMLComponentHierarchy.PrivateInnerComponentType[private_inner_component_list.Count];
                private_inner_component_list.CopyTo(ctree_xml.private_inner_component, 0);
            }

            if (public_inner_component_list.Count > 0)
            {
                ctree_xml.public_inner_component = new XMLComponentHierarchy.PublicInnerComponentType[public_inner_component_list.Count];
                public_inner_component_list.CopyTo(ctree_xml.public_inner_component, 0);
            }

            if (unit_list.Count > 0)
            {
                ctree_xml.unit = new Instantiator.UnitMappingType[unit_list.Count];
                unit_list.CopyTo(ctree_xml.unit, 0);
            }

			if (unit_list_2.Count > 0)
			{
				ctree_xml.unit_2 = new Instantiator.UnitMappingType[unit_list_2.Count];
				unit_list_2.CopyTo(ctree_xml.unit_2, 0);
			}
			
            if (facet_list.Count > 0)
            {
                ctree_xml.facet_list = new XMLComponentHierarchy.FacetType[facet_list.Count];
                facet_list.CopyTo(ctree_xml.facet_list, 0);
            }

            return ctree_xml;
        }

		private static IDictionary<string, ResolveComponentHierarchy> ctree_cache = new Dictionary<string, ResolveComponentHierarchy>();
		private static IDictionary<string, IList<Thread>> delayed_set_host = new Dictionary<string, IList<Thread>>();

		public static void addToCTreeCache(ResolveComponentHierarchy ctree)
		{
            if (ctree.ID.Equals("teste.workflow"))
                Console.WriteLine();

			ctree_cache[ctree.ID] = ctree;
			if (delayed_set_host.ContainsKey(ctree.ID))
			{
				foreach (Thread t in delayed_set_host[ctree.ID])
				{
					t.Start();
					t.Join();
				}
				delayed_set_host.Remove(ctree.ID);
			}
		}


		public static ResolveComponentHierarchy readComponentHierarchy(ComponentHierarchyType ctree_xml, string port_name)
        {
            Console.WriteLine("READ COMPONENT HIERARCHY {0} {1}", ctree_xml.id, port_name);

            IDictionary<string, ResolveComponentHierarchy> memory = new Dictionary<string, ResolveComponentHierarchy>();
            ResolveComponentHierarchyImpl ctree = new ResolveComponentHierarchyImpl(/*context,*/ ctree_xml.id);
            Console.WriteLine("NEW ResolveHierarchyImpl {0}", ctree.GetHashCode());

			addToCTreeCache(ctree);

			readComponentHierarchyNode(ctree, ctree_xml);
            ctree.PortName = port_name;

            readComponentHierarchy1(ctree_xml, ctree);
            readComponentHierarchy2(ctree_xml, ctree);

            return ctree;
        }

        private static void readComponentHierarchyNode(ResolveComponentHierarchyImpl ctree, ComponentHierarchyType ctree_xml)
        {
            ctree.ComponentChoice = new string[ctree_xml.component_choice_2 == null ? 1 : 2];

            ctree.ComponentChoice[0] = ctree_xml.component_choice;
            if (ctree_xml.component_choice_2 != null)
                ctree.ComponentChoice[1] = ctree_xml.component_choice_2;

            ctree.Kind = Constants.kindInt[ctree_xml.kind];

            if (ctree_xml is XMLComponentHierarchy.PrivateInnerComponentType)
                ctree.PortName = ((XMLComponentHierarchy.PrivateInnerComponentType)ctree_xml).id_inner;


            ctree.UnitMapping = new Instantiator.UnitMappingType[ctree_xml.unit_2 == null ? 1 : 2][];

            ctree.UnitMapping[0] = new Instantiator.UnitMappingType[ctree_xml.unit.Length];
            {
                int i = 0;
                foreach (Instantiator.UnitMappingType unit_mapping_x in ctree_xml.unit)
                {
                    Console.WriteLine("CLASS NAME: {0}", unit_mapping_x.class_name);
                    ctree.UnitMapping[0][i++] = unit_mapping_x;
                }
            }

            if (ctree_xml.unit_2 != null)
            {
                ctree.UnitMapping[1] = new Instantiator.UnitMappingType[ctree_xml.unit_2.Length];
                int i = 0;
                foreach (Instantiator.UnitMappingType unit_mapping_x in ctree_xml.unit_2)
                {
                    Console.WriteLine("CLASS NAME: {0}", unit_mapping_x.class_name);
                    ctree.UnitMapping[1][i++] = unit_mapping_x;
                }
            }

            int maxf = 0;
            foreach (FacetType f in ctree_xml.facet_list)
                maxf = f.facet_instance > maxf ? f.facet_instance : maxf;

            ctree.FacetList = new List<int>[maxf + 1];
            for (int j = 0; j <= maxf; j++)
                ctree.FacetList[j] = new List<int>();

            Console.WriteLine("readComponentHierarchyNode -------");
            foreach (FacetType f in ctree_xml.facet_list)
            {
                if (f.facet != null)
                    foreach (int ff in f.facet)
                        ctree.FacetList[f.facet_instance].Add(ff);
            }

        }

        private static void readComponentHierarchy1(XMLComponentHierarchy.ComponentHierarchyType ctree_xml, ResolveComponentHierarchy ctree)
        {
            if (ctree_xml.private_inner_component != null)
                foreach (XMLComponentHierarchy.PrivateInnerComponentType inner_ctree_xml in ctree_xml.private_inner_component)
                {
                    ctree.IsPrivate[inner_ctree_xml.id_inner] = true;
                    ResolveComponentHierarchyImpl inner_ctree = new ResolveComponentHierarchyImpl(/*context,*/ inner_ctree_xml.id);
                    Console.WriteLine("NEW ResolveHierarchyImpl {0}", inner_ctree.GetHashCode());
                    readComponentHierarchyNode(inner_ctree, inner_ctree_xml);

					if (inner_ctree_xml.facet == null)
						inner_ctree_xml.facet = new int[0];

					addToCTreeCache(inner_ctree);

					readComponentHierarchy1(inner_ctree_xml, inner_ctree);

                    ctree.addInnerComponent(inner_ctree_xml.id_inner, new Tuple<ResolveComponentHierarchy, int[]>(inner_ctree, inner_ctree_xml.facet)); // TODO: read facets !!!!
                }
        }

        private static void readComponentHierarchy2(XMLComponentHierarchy.ComponentHierarchyType ctree_xml, ResolveComponentHierarchy ctree)
        {
            Console.WriteLine("readComponentHierarchy2 - INNER {0}", ctree.ID);
            if (ctree_xml.private_inner_component != null)
                foreach (XMLComponentHierarchy.PrivateInnerComponentType inner_ctree_xml in ctree_xml.private_inner_component)
                {
                    ResolveComponentHierarchy inner_ctree = null;
                    Thread t = new Thread(new ThreadStart(() =>
                    {
                        inner_ctree = ctree_cache[inner_ctree_xml.id];
                        readComponentHierarchy2(inner_ctree_xml, inner_ctree);
                    }));
                    if (ctree_cache.ContainsKey(inner_ctree_xml.id))
                    {
                        t.Start();
                        t.Join();
                    }
                    else
                        addToDelayedSetHost(inner_ctree_xml.id, t);
                }

            Console.WriteLine("readComponentHierarchy2 - PUBLIC {0}", ctree.ID);
            if (ctree_xml.public_inner_component != null)
                foreach (PublicInnerComponentType s in ctree_xml.public_inner_component)
                {
                    ctree.IsPrivate[s.id_inner] = false;

                    Console.WriteLine("readComponentHierarchy2 - 1.1 {0} {1}", s.id_inner, s.inner == null);
                    foreach (PublicInnerComponentIdType inner_host_id in s.inner)
                    {
                        Console.WriteLine("readComponentHierarchy2 - 1.2 {0}", inner_host_id.id);
                        Thread t = new Thread(new ThreadStart(() =>
                        {
                            Console.WriteLine("readComponentHierarchy2 - 1 --- {0} {1}", inner_host_id, ctree_cache.ContainsKey(inner_host_id.id));
                            ResolveComponentHierarchy host_inner = ctree_cache[inner_host_id.id];
                            ctree.addInnerComponent(s.id_inner, new Tuple<ResolveComponentHierarchy, int[]>(host_inner, inner_host_id.facet));
                        }));
                        if (ctree_cache.ContainsKey(inner_host_id.id))
                        {
                            Console.WriteLine("readComponentHierarchy2 - 1.3 {0}", inner_host_id.id);
                            t.Start();
                            t.Join();
                            Console.WriteLine("readComponentHierarchy2 - 1.4 {0}", inner_host_id.id);
                        }
                        else
                        {
                            Console.WriteLine("readComponentHierarchy2 - 1.5 {0}", inner_host_id.id);
                            addToDelayedSetHost(inner_host_id.id, t);
                            Console.WriteLine("readComponentHierarchy2 - 1.6 {0}", inner_host_id.id);
                        }
                    }
                }

            Console.WriteLine("readComponentHierarchy2 - HOST {0}", ctree.ID);
            if (ctree_xml.host_component != null)
            {
                foreach (StringListType host_id in ctree_xml.host_component)
                {
                    string id_host = host_id.id[0]; ;
                    Thread t = new Thread(new ThreadStart(() =>
                    {
                    //foreach (string id in host_id.id)
                    //{
                        Console.WriteLine("readComponentHierarchy2 - 2 --- {0} {1}", id_host, ctree_cache.ContainsKey(id_host));
                        ResolveComponentHierarchy host_inner = ctree_cache[id_host];
                        ctree.addHostComponent(host_id.id_inner, host_inner);
                    //}
                }));

                    if (ctree_cache.ContainsKey(id_host))
                    {
                        t.Start();
                        t.Join();
                    }
                    else
                        addToDelayedSetHost(id_host, t);
                }
            }

            Console.WriteLine("ADDED {0} HOSTS TO {1} #{2}", ctree.HostComponents.Count, ctree.ID, ctree.GetHashCode());

        }

        private static void addToDelayedSetHost(string v, Thread t)
        {
            Console.WriteLine("addToDelayedSetHost {0}", v);
            IList<Thread> lt;
            if (!delayed_set_host.TryGetValue(v, out lt))
            {
                lt = new List<Thread>();
                delayed_set_host[v] = lt;
            }
            lt.Add(t);
        }
    }

}
