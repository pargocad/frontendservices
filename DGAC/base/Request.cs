/* Copyright (C) 2007  The Trustees of Indiana University
 *
 * Use, modification and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 *  
 * Authors: Douglas Gregor
 *          Andrew Lumsdaine
 */
using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;

namespace MPI
{

    /// <summary>
    /// A non-blocking communication request.
    /// </summary>
    /// <remarks>
    /// Each request object refers to a single
    /// communication operation, such as non-blocking send 
    /// (see <see cref="Communicator.ImmediateSend&lt;T&gt;(T, int, int)"/>)
    /// or receive. Non-blocking operations may progress in the background, and can complete
    /// without any user intervention. However, it is crucial that outstanding communication
    /// requests be completed with a successful call to <see cref="Wait"/> or <see cref="Test"/>
    /// before the request object is lost.
    /// </remarks>
    public abstract class Request
    {
        /// <summary>
        /// Wait until this non-blocking operation has completed.
        /// </summary>
        /// <returns>
        ///   Information about the completed communication operation.
        /// </returns>
        public abstract CompletedStatus Wait();

        /// <summary>
        /// Determine whether this non-blocking operation has completed.
        /// </summary>
        /// <returns>
        /// If the non-blocking operation has completed, returns information
        /// about the completed communication operation. Otherwise, returns
        /// <c>null</c> to indicate that the operation has not completed.
        /// </returns>
        public abstract CompletedStatus Test();

        /// <summary>
        /// Cancel this communication request.
        /// </summary>
        public abstract void Cancel();
    }


}
