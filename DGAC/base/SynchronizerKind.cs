﻿// /home/jefferson/projetos/appexample/appexample/kinds/SynchronizerKind.cs created with MonoDevelop
// User: jefferson at 13:20 29/5/2008
//
// To change standard headers go to Edit->Preferences->Coding->Standard Headers
//

using System;
using org.hpcshelf.unit;
using gov.cca.ports;

namespace org.hpcshelf.kinds
{

    public interface ISynchronizerKind : IActivateKind, GoPort
	{
	}
	
	public abstract class Synchronizer : Activate, ISynchronizerKind {
	   
	}
	
}
