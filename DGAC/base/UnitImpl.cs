﻿// /home/jefferson/projetos/appexample/appexample/kinds/DataKind.cs created with MonoDevelop
// User: jefferson at 11:14 30/5/2008
//
// To change standard headers go to Edit->Preferences->Coding->Standard Headers
//

using System;
using System.Collections.Generic;
using gov.cca;
using org.hpcshelf.DGAC.utils;
using org.hpcshelf.ports;
using System.Runtime.CompilerServices;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Data;

namespace org.hpcshelf.unit
{

    [DataContract]
    public abstract class Unit : IUnit
    {
		private bool trace_flag = false;

		public bool TraceFlag 
		{ 
			get { return trace_flag; }  
			set { trace_flag = value; } 
		}

       	public Unit()
        {
        }

        private gov.cca.Services services = null;

        /* The context object contains DGAC services. It will be shared by all slices of an application, passed downwards the hierarchy */
        public gov.cca.Services Services { get { return services; } }


        public void setServices(gov.cca.Services services)
        {
            this.services = services;
			
            Console.WriteLine("---- BEFORE REGISTERING USES PORT {0} {1} {2}", this.ThisFacet, this.Id_unit, this.CID);

            foreach (string portName in PortNames)
                services.registerUsesPort(portName, "", new TypeMapImpl());

            services.addProvidesPort(this, Constants.DEFAULT_PROVIDES_PORT_IMPLEMENTS, this.ClassName, new TypeMapImpl());
			
			InitializePort initialize_port_wrapper = this; 
            services.addProvidesPort(initialize_port_wrapper, Constants.INITIALIZE_PORT_NAME, Constants.INITIALIZE_PORT_TYPE, new TypeMapImpl());

            InitializePort release_port_wrapper = this;
            services.addProvidesPort(release_port_wrapper, Constants.RELEASE_PORT_NAME, Constants.RELEASE_PORT_TYPE, new TypeMapImpl());
        }

        public virtual void release1()
        {
            //Console.WriteLine("release1 {0}", Constants.RECONFIGURE_PORT_NAME);
            //services.removeProvidesPort(Constants.RECONFIGURE_PORT_NAME);
            Console.WriteLine("release1 {0}", Constants.INITIALIZE_PORT_NAME);
            services.removeProvidesPort(Constants.INITIALIZE_PORT_NAME);
            Console.WriteLine("release1 {0}", Constants.DEFAULT_PROVIDES_PORT_IMPLEMENTS);
            services.removeProvidesPort(Constants.DEFAULT_PROVIDES_PORT_IMPLEMENTS);

            // foreach (string portName in PortNames)
            // {
            //     Console.WriteLine("release1 {0}", portName);
            //     services.releasePort(portName);
            // }
        }

        public virtual void release2()
        {
            foreach (string portName in PortNames)
            {
                Console.WriteLine("release2 {0} unregisterUsesPort", portName);
                services.unregisterUsesPort(portName);
            }

            Console.WriteLine("release2 {0} removeProvidesPort", Constants.RELEASE_PORT_NAME);
            services.removeProvidesPort(Constants.RELEASE_PORT_NAME);
        }


        public void addSlice(IUnit slice, string portName)
        {
            if (slice_map == null) 
				slice_map = new Dictionary<string,IUnit>();
			if (portName != null && !slice_map.ContainsKey(portName))
			{
            	slice_map.Add(portName, slice);
			}
        }

		public void addHost(IUnit host)
		{
            host_list.Add(host);
		}
		
        virtual public void destroySlice()
        {
        }

		#region IUnit Members

		[MethodImpl(MethodImplOptions.Synchronized)]
		virtual public void on_initialize()
		{
			Console.WriteLine("VIRTUAL ON INITIALIZE");
		}

		[MethodImpl(MethodImplOptions.Synchronized)]
		virtual public void after_initialize()
		{
			Console.WriteLine("VIRTUAL AFTER INITIALIZE");
		}



		public bool IsParallelUnit {get { return false; }}
		public bool IsSingletonUnit {get { return true; }}

		#region facet configuration

		private int this_facet;
		public int ThisFacet { get { return this_facet; } set {this_facet = value; } }

		private int this_facet_instance;
		public int ThisFacetInstance { get { return this_facet_instance; } set {this_facet_instance = value; } }

		private IDictionary<int,int[]> facet_indexes = new Dictionary<int, int[]>(); 
		public IDictionary<int,int[]> FacetIndexes { get { return facet_indexes; } }

		private IDictionary<int,int> facet_multiplicity = new Dictionary<int, int>(); 
		public IDictionary<int,int> FacetMultiplicity { get { return facet_multiplicity; } }

		private IDictionary<int, IDictionary<string,int>> unit_size_in_facet = new Dictionary<int, IDictionary<string, int>>();
		public IDictionary<int, IDictionary<string,int>> UnitSizeInFacet { get { return unit_size_in_facet; } }

		#endregion

		public void configure_facet_topology(Instantiator.UnitMappingType[] unit_mapping)
		{
			IDictionary<int, IList<int>> facet_instances_list = new Dictionary<int, IList<int>>();

			foreach (Instantiator.UnitMappingType unit_mapping_item in unit_mapping)
				if (unit_mapping_item.facet_instance >= 0)
				{
					int facet = unit_mapping_item.facet;
					int facet_instance = unit_mapping_item.facet_instance;
					IList<int> facet_instance_list = null;
					if (!facet_instances_list.TryGetValue(facet, out facet_instance_list))
					{
						facet_instance_list = new List<int>();
						facet_instances_list.Add(facet, facet_instance_list);
					}
					facet_instance_list.Add(facet_instance);
				}

			Console.WriteLine("{1} - facet_instance={0}, configure_facet_topology 2 - " + facet_instances_list.Count, this_facet_instance, this.GetHashCode());

			foreach (KeyValuePair<int, IList<int>> f in facet_instances_list)
			{
				int facet = f.Key;
				IList<int> facet_instance_list = f.Value;
				this.FacetMultiplicity.Add(facet, facet_instance_list.Count);
				int[] facet_instance_array = new int[FacetMultiplicity[facet]];
				facet_instance_list.CopyTo(facet_instance_array, 0);
				this.FacetIndexes.Add(facet, facet_instance_array);
			}

			// DEBUG ...
			foreach (KeyValuePair<int, int[]> kv in this.FacetIndexes)
			{
				Console.Write("{1} - facet_instance={0}, configure_facet_topology - facet=" + kv.Key + ": ", this_facet_instance, this.GetHashCode());
				foreach (int v in kv.Value)
					Trace.Write(v + " ");
				Console.WriteLine(".");
			}

			//DEBUG ...
			foreach (KeyValuePair<int, int> kv in this.FacetMultiplicity)
			{
				Console.WriteLine("{1} - facet_instance={0}, configure_facet_topology - facet=" + kv.Key + " - size=" + kv.Value, this_facet_instance, this.GetHashCode());
			}

            foreach (Instantiator.UnitMappingType unit_mapping_item in unit_mapping)
                if (unit_mapping_item.facet_instance >= 0)
                {
                    int facet_instance = unit_mapping_item.facet_instance;
                    int facet = unit_mapping_item.facet;
                    string unit_id = unit_mapping_item.unit_id;
                    int size = unit_mapping_item.node.Length;
                    IDictionary<string, int> dict;
                    Console.WriteLine("{1} - facet_instance={0}, configure_facet_topology --- facet=" + facet + " / facet_instance=" + facet_instance + " / unit_id=" + unit_id + " / size=" + size, this_facet_instance, this.GetHashCode());
                    if (!this.UnitSizeInFacet.TryGetValue(facet_instance, out dict))
                    {
                        dict = new Dictionary<string, int>();
                        this.UnitSizeInFacet[facet_instance] = dict;
                    }
                    dict[unit_id] = size;
                }

			// DEBUG ...
			foreach (KeyValuePair<int, IDictionary<string, int>> r in UnitSizeInFacet)
				foreach (KeyValuePair<string, int> s in r.Value)
					Console.WriteLine("{1} - facet_instance={0},  UnitSizeInFacet: facet_instance = " + r.Key + " --  unit_id = " + s.Key + " -- size = " + s.Value, this_facet_instance, this.GetHashCode());
		}


		/*public void configure_facet_topology(Instantiator.UnitMappingType unit_mapping_item)
		{
			IDictionary<int, IList<int>> facet_instances_list = new Dictionary<int, IList<int>>();

            {
                int facet = unit_mapping_item.facet;
                int facet_instance = unit_mapping_item.facet_instance;
                IList<int> facet_instance_list = null;
                if (!facet_instances_list.TryGetValue(facet, out facet_instance_list))
                {
                    facet_instance_list = new List<int>();
                    facet_instances_list.Add(facet, facet_instance_list);
                }
                facet_instance_list.Add(facet_instance);
            }

			Console.WriteLine("{1} - facet_instance={0}, configure_facet_topology 2 - " + facet_instances_list.Count, this_facet_instance, this.GetHashCode());

			foreach (KeyValuePair<int, IList<int>> f in facet_instances_list)
			{
				int facet = f.Key;
				IList<int> facet_instance_list = f.Value;
				this.FacetMultiplicity.Add(facet, facet_instance_list.Count);
				int[] facet_instance_array = new int[FacetMultiplicity[facet]];
				facet_instance_list.CopyTo(facet_instance_array, 0);
				this.FacetIndexes.Add(facet, facet_instance_array);
			}

			// DEBUG ...
			foreach (KeyValuePair<int, int[]> kv in this.FacetIndexes)
			{
				Console.Write("{1} - facet_instance={0}, configure_facet_topology - facet=" + kv.Key + ": ", this_facet_instance, this.GetHashCode());
				foreach (int v in kv.Value)
					Trace.Write(v + " ");
				Console.WriteLine(".");
			}

			//DEBUG ...
			foreach (KeyValuePair<int, int> kv in this.FacetMultiplicity)
			{
				Console.WriteLine("{1} - facet_instance={0}, configure_facet_topology - facet=" + kv.Key + " - size=" + kv.Value, this_facet_instance, this.GetHashCode());
			}

			{
				int facet_instance = unit_mapping_item.facet_instance;
				int facet = unit_mapping_item.facet;
				string unit_id = unit_mapping_item.unit_id;
				int size = unit_mapping_item.node.Length;
				IDictionary<string, int> dict;
				Console.WriteLine("{1} - facet_instance={0}, configure_facet_topology --- facet=" + facet + " / facet_instance=" + facet_instance + " / unit_id=" + unit_id + " / size=" + size, this_facet_instance, this.GetHashCode());
				if (!this.UnitSizeInFacet.TryGetValue(facet_instance, out dict))
				{
					dict = new Dictionary<string, int>();
					this.UnitSizeInFacet[facet_instance] = dict;
				}
				dict[unit_id] = size;
			}

		}
*/
		private ComponentID cid = null;

        public ComponentID CID 
		{ 
			set { this.cid = value; }
            get { return cid; } 
		}

		protected string id_interface;

        public string Id_unit
        {
            get { return id_interface; }
            set { id_interface = value; }
        }
		private string qualified_type_name = null;
		
		public string QualifiedComponentTypeName
		{
			get { return qualified_type_name; }			
			set { this.qualified_type_name = value; }
		}
		
		private IDictionary<string, IUnit> slice_map = new Dictionary<string, IUnit>();

		public IDictionary<string, IUnit> Slice 
		{
			get { return slice_map; }
		}

		private IList<IUnit> host_list = new List<IUnit>();

		public IList<IUnit> Hosts
		{
			get { return host_list; }
		}

        private string[] portNames = null;

		public string[] PortNames 
		{
			get { return this.portNames; }
			set { this.portNames = value; }
		}

		private string class_name = null;


		public string ClassName 
		{
			get { return this.class_name; }
			set { this.class_name = value; }
		}
				
		private int kind = -1;
		
		public int Kind
		{
			get { return this.kind; }
			set { this.kind = value;}
		}
		
		#endregion

		#region ReconfigurationAdvicePort implementation
		virtual public void changePort (string portName) 
		{ 
			Console.WriteLine("CHANGE PORT " + portName + " OF " + CID);
			Slice[portName] = (IUnit) services.getPort (portName);
		}
        #endregion



    }


    [DataContract]
    public class ReleasePortWrapper : ReleasePort
    {
        public delegate void MTH();

        private MTH release1_mth;
        private MTH release2_mth;

        [DataMember]
        public MTH Release1Mth { get { return release1_mth; } set { this.release1_mth = value; } }
        public MTH Release2Mth { get { return release2_mth; } set { this.release2_mth = value; } }

        public ReleasePortWrapper()
        {
        }

        public ReleasePortWrapper(MTH release1_mth, MTH release2_mth)
        {
            this.release1_mth = release1_mth;
            this.release2_mth = release2_mth;
        }
        #region AutomaticSlicesPort implementation
        public void release1()
        {
            Console.WriteLine("BEGIN RELEASE 1");
            this.release1_mth();
            Console.WriteLine("END RELEASE 1");
        }

        public void release2()
        {
            Console.WriteLine("BEGIN RELEASE 2");
            this.release2_mth();
            Console.WriteLine("END RELEASE 2");
        }

        #endregion
    }
}
