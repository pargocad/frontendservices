﻿/*=============================================================
(c) all rights reserved
================================================================*/

using gov.cca;
using org.hpcshelf.unit;
using System;
using System.ServiceModel;

namespace org.hpcshelf.ports
{
	public interface ReconfigurationAdvicePort : Port
    {
		[OperationContract]
        void changePort(string portName);
    }

}
