﻿/*=============================================================
(c) all rights reserved
================================================================*/

using gov.cca;
using org.hpcshelf.unit;
using System.ServiceModel;

namespace org.hpcshelf.ports
{
    [ServiceContract]
    [ServiceKnownType(typeof(ReleasePortWrapper))]
    public interface ReleasePort : Port
    {
        [OperationContract]
        void release1();
        void release2();
    }

    [ServiceContract]
    [ServiceKnownType(typeof(ReleasePortWrapper))]
    public interface MultipleReleasePort : ReleasePort
    {
        void addPort(ReleasePort port);
    }

}
