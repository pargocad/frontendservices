﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;

namespace TestServiceClient
{
		[ServiceContract]
		public interface IMathService
		{
			[OperationContract]
			double AddNumber(double dblX, double dblY);
			[OperationContract]
			double SubtractNumber(double dblX, double dblY);
			[OperationContract]
			double MultiplyNumber(double dblX, double dblY);
			[OperationContract]
			double DivideNumber(double dblX, double dblY);
		}


    class MainClass
    {
        public static void Main(string[] args)
        {
            Evaluate("localhost","HTTP",4867,"ADD", 1.0, 2.0);


		}

		private static void Evaluate(string strServer, string strBinding,
			  int nPort, string strOper, double dblVal1, double dblVal2)
		{
			ChannelFactory<IMathService> channelFactory = null;
			EndpointAddress ep = null;

			string strEPAdr = "http://" + strServer +
				   ":" + nPort.ToString() + "/MathService";
			try
			{
				switch (strBinding)
				{
					case "TCP":
						NetTcpBinding tcpb = new NetTcpBinding();
						channelFactory = new ChannelFactory<IMathService>(tcpb);

						// End Point Address
						strEPAdr = "net.tcp://" + strServer + ":" +
									 nPort.ToString() + "/MathService";
						break;

					case "HTTP":
						BasicHttpBinding httpb = new BasicHttpBinding();
						channelFactory = new ChannelFactory<IMathService>(httpb);

						// End Point Address
						strEPAdr = "http://" + strServer + ":" +
								  nPort.ToString() + "/MathService";
						break;
				}

				// Create End Point
				ep = new EndpointAddress(strEPAdr);

				// Create Channel
				IMathService mathSvcObj = channelFactory.CreateChannel(ep);
				double dblResult = 0;

				// Call Methods
				switch (strOper)
				{
					case "ADD": dblResult = mathSvcObj.AddNumber(dblVal1, dblVal2); break;
					case "SUB": dblResult = mathSvcObj.SubtractNumber(dblVal1, dblVal2); break;
					case "MUL": dblResult = mathSvcObj.MultiplyNumber(dblVal1, dblVal2); break;
					case "DIV": dblResult = mathSvcObj.DivideNumber(dblVal1, dblVal2); break;
				}

				//  Display Results.
				Console.WriteLine("Operation {0} ", strOper);
				Console.WriteLine("Operand 1 {0} ", dblVal1.ToString("F2"));
				Console.WriteLine("Operand 2 {0} ", dblVal2.ToString("F2"));
				Console.WriteLine("Result {0} ", dblResult.ToString("F2"));
				channelFactory.Close();
			}
			catch (Exception eX)
			{
				// Something unexpected happended .. 
				Console.WriteLine("Error while performing operation [" +
				  eX.Message + "] \n\n Inner Exception [" + eX.InnerException + "]");
			}
		}
	}
}
