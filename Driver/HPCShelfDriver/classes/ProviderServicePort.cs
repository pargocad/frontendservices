﻿using System;
using System.Collections.Generic;

namespace HPCShelfDriver
{
    internal class ProviderServicePort: ServicePort, IProviderServicePort
    {
        internal ProviderServicePort(IServiceSolutionComponent component, string name, int facet, int index) : base(component, name, facet, index)        
        {
        }
    }
}
