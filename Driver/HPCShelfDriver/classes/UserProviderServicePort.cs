﻿using System;
using System.Collections.Generic;

namespace HPCShelfDriver
{
    internal class UserProviderServicePort: ServicePort, IUserProviderServicePort
    {
        internal UserProviderServicePort(IServiceSolutionComponent component, string name, int facet, int index/*, IDictionary<string, object> contract*/) : base(component, name, facet, index/*, contract*/)        {
        }
    }
}
