﻿using System.Collections.Generic;
using org.hpcshelf.backend.platform;
using org.hpcshelf.core;
using org.hpcshelf.DGAC.utils;

namespace HPCShelfDriver
{
    internal sealed class PlatformSAFe: VirtualPlatform, IPlatformSAFe
    {
		private RootPlatformServices platform_services;
        internal PlatformSAFe(ISystem system_component, CoreServices core_services) : base(system_component, Constants.PLATFORM_SAFE_ID, new Dictionary<string, object>(), core_services)
        {
            platform_services = RootPlatformServices.SingleSAFePlatformServices;
			platform_services.Address = "http://127.0.0.1/PlatformServices.asmx";
            createComponent();
		}
	}
}
