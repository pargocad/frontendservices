﻿using System;
using System.Collections.Generic;
using org.hpcshelf.core;

namespace HPCShelfDriver
{
    internal abstract class ActionComponent: ServiceSolutionComponent, IActionComponent
    {
        public IDictionary<string, IActionPort[]> ActionPort => new Dictionary<string, IActionPort[]>(action_port);

        private readonly IDictionary<string, IActionPort[]> action_port = new Dictionary<string,IActionPort[]>();

        protected virtual void addActionPort(IActionPort[] port)
        {
            string name = port[0].Name;
            this.port[name] = action_port[name] = port;
        }

        protected ActionComponent(ISystem system_component, string cRef, IDictionary<string, object> contract, CoreServices core_services, Tuple<int, IVirtualPlatform>[] placement) : base(system_component, cRef, contract, core_services, placement)
        {
        }

        protected virtual void populateActionPorts(Tuple<string, int, int, string>[] l)
        {
            string id_inner = null;
            IList<IActionPort> action_port_list = new List<IActionPort>();
            if (l.Length > 0)
            {
                foreach (Tuple<string, int, int, string> e in l)
                    if (e.Item4.Equals("peer"))
                    {
                        if (id_inner == null)
                        {
                            id_inner = e.Item1;
                        }
                        else if (!id_inner.Equals(e.Item1))
                        {
                            action_port[id_inner] = new IActionPort[action_port_list.Count];
                            action_port_list.CopyTo(action_port[id_inner], 0);
                            action_port_list = new List<IActionPort>();
                            id_inner = e.Item1;
                        }
                        action_port_list.Add(new ActionPort(this, id_inner, e.Item3, e.Item2));
                    }
                if (id_inner != null)
                {
                    action_port[id_inner] = new IActionPort[action_port_list.Count];
                    action_port_list.CopyTo(action_port[id_inner], 0);
                }
            }
		}
    }
}
