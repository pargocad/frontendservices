﻿using System;
using System.Collections.Generic;
using org.hpcshelf.core;

namespace HPCShelfDriver
{
    internal sealed class Computation : ActionComponent, IComputation
    {
        readonly IVirtualPlatform placement;

        internal Computation(ISystem system_component, string cRef, IDictionary<string, object> contract, IVirtualPlatform placement, CoreServices core_services) : base(system_component, cRef, contract, core_services, new Tuple<int, IVirtualPlatform>[1] { new Tuple<int, IVirtualPlatform>(0, placement) })
        {
            this.placement = placement; 
            placement.addComponent(this);
			createComponent();
			((Workflow)system_component.Workflow).addLifeCyclePort(this);
		}

        protected override void populatePorts(Tuple<string, int, int, string>[] ports)
        {
            populateServicePorts(ports);
            populateActionPorts(ports);
		}

		public IVirtualPlatform VirtualPlatform => placement;
    }
}
