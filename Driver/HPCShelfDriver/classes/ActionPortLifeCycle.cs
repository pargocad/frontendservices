﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using org.hpcshelf.core;
using org.hpcshelf.DGAC;
using org.hpcshelf.DGAC.utils;
using org.hpcshelf.kinds;
using gov.cca;

namespace HPCShelfDriver
{
    internal class ActionPortLifeCycle : ActionPort, IActionPortLifeCycle
    {
        private CoreServices core_services;

        private string component_name = null;
        public string ComponentName => component_name;

        public ActionPortLifeCycle(IActionComponent component, string component_name, string port_name, CoreServices core_services) : base(component, port_name, 0, 0)
        {
            this.core_services = core_services;
            this.component_name = component_name;
        }

		public void null_reaction() { }

		public delegate void PerformLifeCycleAction(string s);

		void perform_life_cycle_action(string port_name, out IActionFuture future, Action reaction, PerformLifeCycleAction perform)
		{
			ManualResetEvent sync = new ManualResetEvent(false);
			ActionFuture future_ = new ActionFuture(sync);
			future = future_;
            Thread t = new Thread(() =>
            {
                try
                {
                    perform(port_name);
                    sync.Set();
                    future_.setCompleted();
                    reaction();
                }
                catch (Exception e)
                {
                    sync.Set();
                    future_.setCompleted();
                    future_.Failed = e;
                }
            }
            );
			t.Start();
		}

        void perform_life_cycle_action(string port_name, PerformLifeCycleAction perform)
        {
            perform(port_name);
        }

        private void perform_resolve(string instance_name)
        {
            Console.WriteLine("resolve {0}", instance_name);
            core_services.resolve(instance_name);
        }

		private void perform_resolve_system(string instance_name)
		{
			Console.WriteLine("resolve system {0}", instance_name);
			core_services.resolve_system(instance_name);
		}
		
        private void try_certify(string instance_name)
        {
            // CERTIFICATION PROTOCOL: TODO !!!
            Console.WriteLine("certify {0}", instance_name);
            core_services.certify(instance_name);
        }

        private void perform_deploy(string instance_name)
        {
            Console.WriteLine("deploy {0}", instance_name);
            core_services.deploy(instance_name);
        }

        private void perform_instantiate(string instance_name)
        {
            Console.WriteLine("instantiate {0}", instance_name);
            core_services.instantiate(instance_name);
        }

        private void perform_run(string instance_name)
        {
          //  Thread t = new Thread(new ThreadStart(delegate
          //   {
                 Console.WriteLine("run {0} !!!", instance_name);
                 core_services.run(instance_name);
          //   }));
          //  t.Start();
          //  t.Join();
        }

        private void perform_release(string instance_name)
        {
            Console.WriteLine("release {0} !!!", instance_name);
            core_services.release(instance_name);
        }

        public void resolve()
        {
            perform_life_cycle_action(this.ComponentName, perform_resolve);
		}

        public void resolve(out IActionFuture future)
        {
            perform_life_cycle_action(this.ComponentName, out future, null_reaction, perform_resolve);
            if (future.Failed != null) throw future.Failed;
        }

        public void resolve(Action reaction, out IActionFuture future)
        {
			perform_life_cycle_action(this.ComponentName, out future, reaction, perform_resolve);
            if (future.Failed != null) throw future.Failed;
        }

        public void resolve_system()
		{
			perform_life_cycle_action(this.ComponentName, /* out future, null_reaction, */perform_resolve_system);
		}

		public void resolve_system(out IActionFuture future)
		{
			perform_life_cycle_action(this.ComponentName, out future, null_reaction, perform_resolve_system);
            if (future.Failed != null) throw future.Failed;
        }

        public void resolve_system(Action reaction, out IActionFuture future)
		{
			perform_life_cycle_action(this.ComponentName, out future, reaction, perform_resolve_system);
            if (future.Failed != null) throw future.Failed;
        }

        public void certify()
		{
            perform_life_cycle_action(this.ComponentName, try_certify);
		}

		public void certify(out IActionFuture future)
		{
			perform_life_cycle_action(this.ComponentName, out future, null_reaction, try_certify);
            if (future.Failed != null) throw future.Failed;
        }

        public void certify(Action reaction, out IActionFuture future)
		{
			perform_life_cycle_action(this.ComponentName, out future, reaction, try_certify);
            if (future.Failed != null) throw future.Failed;
        }

        public void deploy()
		{
			perform_life_cycle_action(this.ComponentName, perform_deploy);
		}

		public void deploy(out IActionFuture future)
		{
			perform_life_cycle_action(this.ComponentName, out future, null_reaction, perform_deploy);
            if (future.Failed != null) throw future.Failed;
        }

        public void deploy(Action reaction, out IActionFuture future)
		{
			perform_life_cycle_action(this.ComponentName, out future, reaction, perform_deploy);
            if (future.Failed != null) throw future.Failed;
        }

        public void instantiate()
		{
			perform_life_cycle_action(this.ComponentName, perform_instantiate);
		}

		public void instantiate(out IActionFuture future)
		{
			perform_life_cycle_action(this.ComponentName, out future, null_reaction, perform_instantiate);
            if (future.Failed != null) throw future.Failed;
        }

        public void instantiate(Action reaction, out IActionFuture future)
		{
			perform_life_cycle_action(this.ComponentName, out future, reaction, perform_instantiate);
            if (future.Failed != null) throw future.Failed;
        }

        public void run()
		{
			perform_life_cycle_action(this.ComponentName, perform_run);
		}


        private IActionFuture run_future;

        public void start_run()
        {
            perform_life_cycle_action(this.ComponentName, out run_future, null_reaction, perform_run);
        }

        public void wait_run()
        {
            run_future.wait();
        }
        public void run(out IActionFuture future)
		{
			perform_life_cycle_action(this.ComponentName, out future, null_reaction, perform_run);
            if (future.Failed != null) throw future.Failed;
        }

        public void run(Action reaction, out IActionFuture future)
		{
			perform_life_cycle_action(this.ComponentName, out future, reaction, perform_run);
            if (future.Failed != null) throw future.Failed;
        }

        public void release()
		{
			perform_life_cycle_action(this.ComponentName, perform_release);
		}

		public void release(out IActionFuture future)
		{
			perform_life_cycle_action(this.ComponentName, out future, null_reaction, perform_release);
            if (future.Failed != null) throw future.Failed;
        }

        public void release(Action reaction, out IActionFuture future)
		{
			perform_life_cycle_action(this.ComponentName, out future, reaction, perform_release);
            if (future.Failed != null) throw future.Failed;
        }
    }


}
