﻿using System;
using System.Collections.Generic;
using org.hpcshelf.core;

namespace HPCShelfDriver
{
    internal sealed class Connector: ActionComponent, IConnector
    {

        internal Connector(ISystem system_component, string cRef, IDictionary<string, object> contract, CoreServices core_services, Tuple<int, IVirtualPlatform>[] facet_placement) : base(system_component, cRef, contract, core_services, facet_placement)
        {
			createComponent();
			((Workflow)system_component.Workflow).addLifeCyclePort(this);
		}

        protected override void populatePorts(Tuple<string, int, int, string>[] ports)
		{
			populateServicePorts(ports);
			populateActionPorts(ports);
		}
	}
}
