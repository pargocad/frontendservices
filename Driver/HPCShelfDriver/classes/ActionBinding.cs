﻿using System;
using System.Collections.Generic;
using org.hpcshelf.core;

namespace HPCShelfDriver
{
    internal sealed class ActionBinding : Binding, IActionBinding
    {
        readonly IActionPort[] action_port;

        internal ActionBinding(ISystem system_component, string cRef, IDictionary<string, object> contract, IActionPort[] action_port, CoreServices core_services) : base(system_component, cRef, contract, mk(action_port), core_services)
        {
            this.action_port = action_port;
            createComponent();
			((Workflow)system_component.Workflow).addLifeCyclePort(this);
		}

        private static Tuple<IPort, int>[] mk(IActionPort[] action_port)
        {
            Tuple<IPort, int>[] port_output = new Tuple<IPort, int>[action_port.Length];

            int i = 0;
            foreach (IActionPort port in action_port)
                port_output[i++] = new Tuple<IPort, int>(port, 0);

            return port_output;
        }
    }
}
