﻿﻿using System;
using System.Collections.Generic;

namespace HPCShelfDriver
{
    internal abstract class ServicePort : Port, IServicePort
    {
        protected ServicePort(IServiceSolutionComponent component, string name, int facet, int index/*, IDictionary<string, object> contract*/) : base(component, name, facet, index/*, contract*/)
        {
        }
    }
}
