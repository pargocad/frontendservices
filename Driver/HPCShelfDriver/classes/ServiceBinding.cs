﻿using System;
using System.Collections.Generic;
using org.hpcshelf.core;

namespace HPCShelfDriver
{
    internal sealed class ServiceBinding : Binding, IServiceBinding
    {
        readonly IUserServicePort user_port;
        readonly IProviderServicePort provider_port;

		internal ServiceBinding(ISystem system_component,
								string cRef,
								IDictionary<string, object> contract,
								IUserServicePort user_port,
								IProviderServicePort provider_port, CoreServices core_services)
            : base(system_component, cRef, contract, mk(user_port, provider_port), core_services)
		{
			this.provider_port = provider_port;
			this.user_port = user_port;
			createComponent();
			((Workflow)system_component.Workflow).addLifeCyclePort(this);
		}

        private static Tuple<IPort, int>[] mk(IUserServicePort user_port, IProviderServicePort provider_port)
        {
            if (user_port is IUserProviderServicePort || provider_port is IUserProviderServicePort)
                return new Tuple<IPort, int>[2] { new Tuple<IPort, int>(user_port, 0), new Tuple<IPort, int>(provider_port, 0) };
            else
                return new Tuple<IPort, int>[2] { new Tuple<IPort, int>(user_port, 0), new Tuple<IPort, int>(provider_port, 1) };
        }
    }
}
