﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using org.hpcshelf.core;
using org.hpcshelf.DGAC;
using org.hpcshelf.DGAC.utils;
using org.hpcshelf.kinds;
using gov.cca;

namespace HPCShelfDriver
{
    internal class ActionPortWorkflow : ActionPort, IActionPortWorkflow
    {
        private CoreServices core_services;
        private Services services;

        public ActionPortWorkflow(IActionComponent component, string name, string action_port_type, CoreServices core_services, Services services) : base(component, name, 0, 0)
        {
            this.core_services = core_services;
            this.services = services;
			services.registerUsesPort(name, action_port_type, new TypeMapImpl());
		}

        public void invoke(object action_id)
        {
            string port_name = this.Name;
            string action_name = (string)action_id;

            Console.WriteLine("BEGIN INVOKE port_name={0} action_name={1}", port_name, action_name);

            ITaskBindingKind action_port = port(port_name);
            action_port.invoke(action_name);
        }

        private ITaskBindingKind port(string port_name)
        {
            ITaskBindingKind port_return;

            port_return = (ITaskBindingKind)services.getPort(port_name);

            Console.WriteLine("***** port ---- {0} {1}", port_name, port_return != null);

            port_return.TraceFlag = true;

            return port_return;
        }

        public object invoke(object[] action_name)
        {
            string port_name = this.Name;

			Console.WriteLine("BEGIN INVOKE 0 port_name={0} action_name={1}", port_name, action_name);

			ITaskBindingKind action_port = port(port_name);
            return action_port.invoke(action_name);
        }

        public void invoke(object action_id, out IActionFuture future)
        {
            string port_name = this.Name;
            string action_name = (string)action_id;
            future = null;

            Console.WriteLine("BEGIN INVOKE 1 port_name={0} action_name={1}", port_name, action_name);

            ITaskBindingKind action_port = port(port_name);
            action_port.invoke(action_name, out future);
        }

        public void invoke(object[] action_name, out IActionFuture future)
        {
            string port_name = this.Name;

			Console.WriteLine("BEGIN INVOKE 2 port_name={0} action_name={1}", port_name, action_name);
			
            ITaskBindingKind action_port = port(port_name);
            action_port.invoke(action_name, out future);
        }

        public void invoke(object action_id, Action reaction, out IActionFuture future)
        {
            future = null;
            string port_name = this.Name;
            string action_name = (string)action_id;

			Console.WriteLine("BEGIN INVOKE 3 port_name={0} action_name={1}", port_name, action_name);
			
            ITaskBindingKind action_port = port(port_name);
            action_port.invoke(action_name, reaction, out future);
        }

        public void invoke(object[] action_name, Action[] reaction, out IActionFuture future)
        {
            string port_name = this.Name;

			Console.WriteLine("BEGIN INVOKE 4 port_name={0} action_name={1}", port_name, action_name);
			
            ITaskBindingKind action_port = port(port_name);
            action_port.invoke(action_name, reaction, out future);
        }
    }

    internal class ActionFuture : IActionFuture
    {
		private object action;
		private bool completed = false;
		private ManualResetEvent sync = null;

		public ActionFuture(ManualResetEvent sync)
		{
			this.sync = sync;
		}

        public object Action
		{
			get { return action; }
			set { action = value; }
		}

		public IActionFutureSet createSet()
        {
			IActionFutureSet afs = new ActionFutureSet();
			afs.addAction(this);
			return afs;
		}

		private IList<AutoResetEvent> waiting_sets = new List<AutoResetEvent>();

		public object waiting_lock = new object();

        private Exception exception = null;

        public Exception Failed { get { return exception; } set { exception = value; } }

        public void setCompleted()
		{
			lock (waiting_lock)
			{
				completed = true;
				foreach (AutoResetEvent waiting_set in waiting_sets)
					waiting_set.Set();
			}
		}

        public void registerWaitingSet(AutoResetEvent waiting_set)
        {
            lock (waiting_lock)
            {
                if (completed)
                    waiting_set.Set();
                waiting_sets.Add(waiting_set);
            }
        }

		public void unregisterWaitingSet(AutoResetEvent waiting_set)
		{
			waiting_sets.Remove(waiting_set);
		}

		public bool test()
        {
            return completed;
        }

        public void wait()
        {
			if (!completed)
				sync.WaitOne();
		}
    }

}
