﻿using System;
using System.Collections.Generic;

namespace HPCShelfDriver
{
    public interface IPort
    {
        string Name { get; }
        IBinding Binding { get; }
        IServiceSolutionComponent Component { get; }
        int Index { get; }
		int Facet { get; }
		void setBinding(IBinding binding);
    }
}
