﻿using System;
namespace HPCShelfDriver
{
    public interface IPlacedComponent : IComponent
    {
		IVirtualPlatform VirtualPlatform { get; }
	}
}
