﻿using System;
using System.Collections.Generic;

namespace HPCShelfDriver
{
    public interface IBinding : ISolutionComponent
    {
        Tuple<IPort,int>[] Port { get; }
        IDictionary<IVirtualPlatform, IPort[]> PortPlacement { get; }
    }
}
