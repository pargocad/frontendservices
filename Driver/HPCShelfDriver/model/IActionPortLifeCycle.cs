﻿using System;
using org.hpcshelf.kinds;

namespace HPCShelfDriver
{
    public interface IActionPortLifeCycle : IActionPort
    {
        string ComponentName { get; }

		void resolve_system();
		void resolve_system(out IActionFuture future);
		void resolve_system(Action reaction, out IActionFuture future);

        void resolve();
		void resolve(out IActionFuture future);
		void resolve(Action reaction, out IActionFuture future);

		void certify();
		void certify(out IActionFuture future);
		void certify(Action reaction, out IActionFuture future);

        void deploy();
		void deploy(out IActionFuture future);
		void deploy(Action reaction, out IActionFuture future);

		void instantiate();
		void instantiate(out IActionFuture future);
		void instantiate(Action reaction, out IActionFuture future);

		void run();
		void run(out IActionFuture future);
		void run(Action reaction, out IActionFuture future);

        void start_run();
        void wait_run();

        void release();
		void release(out IActionFuture future);
		void release(Action reaction, out IActionFuture future);
	}
}
