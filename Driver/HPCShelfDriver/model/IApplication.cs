﻿using System;
using System.Collections.Generic;
using org.hpcshelf.kinds;
using gov.cca;

namespace HPCShelfDriver
{
    public interface IApplication : IServiceSolutionComponent
    {
        Services Services { get; }
        IEnvironmentKind Unit { get; set; }

        //IUserServicePort addUserPort(string name, IDictionary<string, object> contract);
		//IProviderServicePort addProviderPort(string name, IDictionary<string, object> contract);
		//IUserProviderServicePort addDirectUserPort(string name, IDictionary<string, object> contract);
		//IUserProviderServicePort addDirectProviderPort(string name, IDictionary<string, object> contract);

		IServicePort NewServicePort { get; }
	}
}
