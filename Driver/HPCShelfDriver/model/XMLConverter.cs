﻿using System;
using System.Collections.Generic;
using System.Globalization;
using org.hpcshelf.DGAC;
using ComponentXML;

namespace HPCShelfDriver.classes
{
    public class XMLConverter
    {
        public static string saveComponent(IComponent c)
        {
            string s;

            IDriverComponent cx = fromIComponentToIDriverComponent(c);

            s = LoaderApp.serialize<IDriverComponent>(cx);

            return s;
        }

/*		public static IComponent loadComponent(string s)
		{
            IDriverComponent cx = LoaderApp.deserialize<IDriverComponent>(s);

			IComponent c = fromIDriverComponentToIComponent(cx);

			return c;
		}

        private static IComponent fromIDriverComponentToIComponent(IDriverComponent cx)
        {
            IComponent c = createComponent(cx.kind);

            return c;
        }

        private static IComponent createComponent(IDriverComponentKind kind)
        {
            if (kind = IDriverComponentKind.Binding)
                return new IBinding();
            else if (kind = IDriverComponentKind.DataSource)
                return new IDataSource();
            else if (kind == IDriverComponentKind.Connector)
                return new IConnector();
            else if (kind == IDriverComponentKind.Computation)
                return new IComputation();
            else if (kind == IDriverComponentKind.VirtualPlatform)
                return new IVirtualPlatform();
            else
                throw new Exception("UNEXPECTED ERROR: Unknown Kind !");
        }
*/

        public static IDriverComponent fromIComponentToIDriverComponent(IComponent c)
        {
            IDriverComponent driver_component = new IDriverComponent();

            driver_component.name = c.Name;
            driver_component.kind = kindOf(c);
            driver_component.library_path = (string) (c.Contract.ContainsKey("name") ? c.Contract["name"] : null);
			driver_component.context_argument = new IDriverContextArgument[c.Contract.Count];

            IDictionary<string, object>  contract = new Dictionary<string,object>(c.Contract);
            contract.Remove("name");

            fillContextArguments(driver_component.context_argument, contract);

            if (c.Placement != null)
            {
                driver_component.placement = new IDriverPlacement[c.Placement.Count];
                fillPlacement(driver_component.placement, c.Placement);
            } 
            else
			    driver_component.placement = new IDriverPlacement[0];

			if (driver_component.kind == IDriverComponentKind.Binding)
            {
                if (((IBinding)c).Port != null)
                {
                    driver_component.port = new IDriverPort[((IBinding)c).Port.Length];
                    fillPorts(driver_component.port, ((IBinding)c).Port);
                }
                else
                    driver_component.port = new IDriverPort[0];
            }

            return driver_component;
        }

        private static void fillPorts(IDriverPort[] driver_port, Tuple<IPort, int>[] port)
        {
			int i = 0;
			foreach (Tuple<IPort, int> p in port)
			{
				driver_port[i] = new IDriverPort();
                driver_port[i].name = p.Item1.Name;
				driver_port[i].facet_host = p.Item1.Facet;
				driver_port[i].facet_binding = p.Item2;
				driver_port[i].index = p.Item1.Index;
                driver_port[i].component = p.Item1.Component.Name;
                driver_port[i].binding = p.Item1.Binding.Name;
                driver_port[i].role = p.Item1 is IUserProviderServicePort ? IDriverPortRole.client_server : p.Item1 is IProviderServicePort ? IDriverPortRole.server :  p.Item1 is IUserServicePort ? IDriverPortRole.client : IDriverPortRole.peer;
                i++;
			}
		}

        private static void fillPlacement(IDriverPlacement[] driver_placement, IDictionary<int, IVirtualPlatform[]> placement)
        {
			int i = 0;
			foreach (KeyValuePair<int, IVirtualPlatform[]> p in placement)
			{
                driver_placement[i] = new IDriverPlacement();
                driver_placement[i].facet = p.Key;

                driver_placement[i].virtual_platform = new string[p.Value.Length];
                for (int j = 0; j < p.Value.Length; j++)
                    driver_placement[i].virtual_platform[j] = p.Value[j].Name;
                i++;
			}
		}

        public static IDriverContract convertContract(IDictionary<string, object> contract)
        {
            IDictionary<string, object> contract_new = new Dictionary<string, object>(contract);
            IDriverContract result = new IDriverContract();
			//            result.Items /*result.context_argument*/ = new object[1]  { new IDriverContextArgument[contract.Count] };
            result.Items = new IDriverContextArgument[contract.Count];;
			result.library_path = contract.ContainsKey("name") ? (string)contract["name"] : null;
            IDictionary<string, object> new_contract = new Dictionary<string, object>(contract);
            new_contract.Remove("name");
            fillContextArguments((ComponentXML.IDriverContextArgument[])result.Items /*result.context_argument*/, new_contract);

            return result;
        }

        private static void fillContextArguments(IDriverContextArgument[] arguments, IDictionary<string, object> contract)
        {
            int i = 0;
            foreach (KeyValuePair<string, object> context_argument in contract)
            {
                arguments[i] = new IDriverContextArgument();
                arguments[i].parameter_id = context_argument.Key;
                if (context_argument.Value is IDictionary<string, object>)
                {
                    IDictionary<string, object> restriction_contract = new Dictionary<string, object>((IDictionary<string, object>)context_argument.Value);
                    arguments[i].library_path = (string)restriction_contract["name"];
                    restriction_contract.Remove("name");
                    IDriverContextArgument[] restriction = new IDriverContextArgument[restriction_contract.Count];
                    arguments[i].Items = restriction;

                    fillContextArguments(restriction, restriction_contract);
                }
                else 
                {
                    arguments[i].Items = new object[1];
                    arguments[i].Items[0] = decimal.Parse(context_argument.Value.ToString(), NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands | NumberStyles.AllowLeadingSign, CultureInfo.InvariantCulture);
                }
                i++;
            }
        }

        private static IDriverComponentKind kindOf(IComponent c)
        {
            if (c is IBinding) return IDriverComponentKind.Binding;
            else if (c is IDataSource) return IDriverComponentKind.DataSource;
            else if (c is IComputation) return IDriverComponentKind.Computation;
            else if (c is IConnector) return IDriverComponentKind.Connector;
            else if (c is IVirtualPlatform) return IDriverComponentKind.VirtualPlatform;
            else if (c is IWorkflow) return IDriverComponentKind.Workflow;
            else if (c is IApplication) return IDriverComponentKind.Application;
            else if (c is ISystem) return IDriverComponentKind.System;
            else throw new Exception("UNEXPECTED ERROR: Unknown Kind !");
        }
    }
}
