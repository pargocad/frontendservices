﻿using System.Collections.Generic;

namespace HPCShelfDriver
{
    public interface IComponent
    {
		IDictionary<string, object> Contract { get; set; }
        string Name { get; }
        IDictionary<int, IVirtualPlatform[]> Placement { get; }
	}
}
