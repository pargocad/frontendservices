﻿using System;
namespace HPCShelfDriver
{
    public interface IVirtualPlatform: ISolutionComponent
    {
        IServiceSolutionComponent[] Component { get; }
        void addComponent(IServiceSolutionComponent component);
    }
}
