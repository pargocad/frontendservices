﻿using System;
using System.Collections.Generic;

namespace HPCShelfDriver
{
    public interface IServiceSolutionComponent : ISolutionComponent
    {
        IDictionary<string, IUserServicePort[]>     UserPort { get; }
        IDictionary<string, IProviderServicePort[]> ProviderPort { get; }
        IDictionary<string, IPort[]>                Port { get; }
        IDictionary<string, IServicePort[]>         ServicePort { get; }
    }
}
