﻿using System;
using org.hpcshelf.kinds;

namespace HPCShelfDriver
{
    public interface IActionPortWorkflow : IActionPort, IActionInvocation
    {

    }
}
