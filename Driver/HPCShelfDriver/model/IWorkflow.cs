﻿using System;
using System.Collections.Generic;
using org.hpcshelf.DGAC;
using org.hpcshelf.kinds;
using gov.cca;
using gov.cca.ports;

namespace HPCShelfDriver
{
    public interface IWorkflow: IActionComponent
    {
        Services Services { get;}
        IWorkflowKind Unit { get; set; }
        GoPort Go { get; set; }

        // IActionPortWorkflow addActionPort(string name, string action_port_type);
        IDictionary<int, IList<IActionPortLifeCycle>> ActionLifeCyclePortByKind { get; }
        IDictionary<string, IActionPortLifeCycle> ActionPortLifeCycle { get; }
        IDictionary<string, IActionPortWorkflow>  ActionPortWorkflow  { get; }

        IActionPort NewActionPort { get; }

        void releaseLifeCyclePort(string name);
    }
}
