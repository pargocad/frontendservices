﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentBindingBase;
using org.hpcshelf.gust.example.pgr.DataPGRANK;
using org.hpcshelf.gust.graph.OutputFormat;
using org.hpcshelf.gust.graph.VertexBasic;
using org.hpcshelf.gust.port.environment.PortTypeDataSinkGraphInterface;
using br.ufc.mdcc.common.KVPair;
using org.hpcshelf.kinds;
using HPCShelfDriver;

namespace DriverTest
{
    class MainClass
    {
        static string OUTPUT_FILE_PATH = System.Environment.GetFolderPath(System.Environment.SpecialFolder.UserProfile) + "/gust.output";//target to folder home
        
        public static void Main(string[] args)
        {
            // 1. CREATING A SYSTEM COMPONENT (i.e. component that represents the parallel computing system)

            // 1.1. Specifying the context signature of the system, as dictionary associating context parameter identifiers to a variable and a bound.
            /* The variable is a string that may be referred in contracts of components included in the system.
             * The bound is a contract. Each contract is a dictionary with at least a "name" key (mandatory), which must be associated with a string 
             * containing the qualified name of the component. The other entries associate context arguments to the component referred in "name". 
             * Each context parameter is referred by its name, and the argument may be defined by a contract or a context variable. In the system contract,
             * the bound of "input_data_host" is a contract for the component "org.hpcshelf.platform.maintainer.DataHost", which does not have any
             * context parameter. Also, it is associated to the variale "MData".
             */
            IDictionary<string, Tuple<string, object>> contract_signature = new Dictionary<string, Tuple<string, object>>()
            {
                {"input_data_host", new Tuple<string, object>("MData", new Dictionary<string,object>() {{"name","org.hpcshelf.platform.maintainer.DataHost"}})}
            };

            ISystem system = SystemBuilder.build("teste", contract_signature);


            // 2. CREATING THE "source" COMPONENT, from which the graph is obtained.

            // 2.1. Specifying the contract of the virtual platform where the source component is placed, named "platform_data_source".
            /* The "maintainer" argument is associated to the value of "MData". */
            IDictionary<string, object> contract_platform_data_source = new Dictionary<string, object>()
            {
                {"name", "org.hpcshelf.platform.Platform"},
                {"maintainer", "MData"} 
            };

            // 2.2. Creating an object that will represent the virtual plataform "platform_data_source"
            IVirtualPlatform platform_data_source = system.newVirtualPlatform("platform_data_source", contract_platform_data_source);

            // 2.3. Creating a set of system context parameters that will be necessary for "source".
            system.addParameter("key_type", "TKey", new Dictionary<string, object> { { "name", "org.hpcshelf.gust.graph.VertexBasic" } });
            system.addParameter("value_type", "TValue", new Dictionary<string, object> { { "name", "org.hpcshelf.gust.graph.DataObject" } });
            system.addParameter("graph_input_format", "GIF", new Dictionary<string, object> { { "name", "org.hpcshelf.gust.graph.InputFormat" } });

            // 2.4. Specifying an argument to the "value_type" context parameter. 
            /* Thus, all references to "TValue" will be associated to the argument. For context parameters that an argument is not defined, 
             * the bound is applied. Notice that the argument must be a subtype of the parameter bound.
             */
            system.setArgument("value_type", new Dictionary<string, object> { { "name", "org.hpcshelf.gust.example.pgr.DataPGRANK" } });

            // 2.5. Specifying the contract of "source".
            /* Notice that all arguments are system context variables, previously defined. */
            IDictionary<string, object> contract_source = new Dictionary<string, object>()
            {
                {"name", "org.hpcshelf.gust.custom.io.DataSourceGraph"},
                {"platform_maintainer", "MData"},
                {"input_key_type", "TKey"},
                {"input_value_type", "TValue"},
                {"graph_input_format", "GIF"}

            };

            // 2.6. Creating an data source object that represent the "source" component.
            IDataSource source = system.newDataSource("source", contract_source, platform_data_source);

            // 3. CREATING THE COMPUTATION COMPONENTS, "reduce_1" and "reducer_2".

            // 3.1. Specifying the contract of the platforms where "reducer_1" and "reducer_2" will be placed, "platform_reduce_1" and "platform_reduce_2".
            IDictionary<string, object> contract_platform_reduce = new Dictionary<string, object>()
            {
                {"name", "org.hpcshelf.platform.Platform"},
                {"maintainer", "MPeer"}
            };

            // 3.2. Adding a new system context parameter for representing the maintainer of the virtual platforms.
            system.addParameter("compute_host", "MPeer", new Dictionary<string, object> { { "name", "org.hpcshelf.platform.maintainer.ComputeHost" } });

            // 3.3. Crating the virtual platform objects that represent "platform_reduce_1" and "platform_reduce_2".
            IVirtualPlatform platform_reduce_1 = system.newVirtualPlatform("platform_reduce_1", contract_platform_reduce);
            IVirtualPlatform platform_reduce_2 = system.newVirtualPlatform("platform_reduce_2", contract_platform_reduce);

            //3.4. Adding new system context parameters that will be referred in the contracts of "reducer_1" and "reducer_2"
            system.addParameter("graph_type", "G", new Dictionary<string, object> { { "name", "br.ufc.mdcc.common.Data" } });
            system.addParameter("action_port_type_reduce", "ATerminationPortType", new Dictionary<string, object> { { "name", "mapreduce.gust.iterative.port.task.TerminationFlagTaskPortType" } });
            system.addParameter("gusty_function", "RF", new Dictionary<string, object>
                                                            {           
                                                                        {"name", "org.hpcshelf.gust.custom.GustyFunction" },
                                                                        {"input_key_type", "TKey"},
                                                                        {"input_value_type", "TValue"},
                                                                        {"output_key_type", "TKey"},
                                                                        {"output_value_type", "TValue"},
                                                                        {"graph_type", "G"},
                                                                        {"graph_input_format", "GIF"},
                                                                        {"action_port_type", "ATerminationPortType"}
                                                            });

            // 3.5. Set the arguments to be applied to two of the recently created context parameters. 
            /* For the other, "action_port_type_reduce", the bound is applied */

            system.setArgument("graph_type", new Dictionary<string, object>
                                                {          {"name", "org.hpcshelf.gust.graph.DirectedGraph"},
                                                           {"vertex_type", new Dictionary<string, object> { { "name", "org.hpcshelf.gust.graph.Vertex" } }},
                                                           {"edge_type", new Dictionary<string, object> { { "name", "org.hpcshelf.gust.graph.EdgeWeighted" }, {"vertex_type", new Dictionary<string, object> { { "name", "org.hpcshelf.gust.graph.Vertex" } } } }},
                                                           {"container", new Dictionary<string, object> { { "name", "org.hpcshelf.gust.graph.container.DataContainerKV" }, {"vertex_type", new Dictionary<string, object> { { "name", "org.hpcshelf.gust.graph.Vertex" } } }, {"edge_type", new Dictionary<string, object> { { "name", "org.hpcshelf.gust.graph.EdgeWeighted" }, { "vertex_type", new Dictionary<string, object> { { "name", "org.hpcshelf.gust.graph.Vertex" } } } } } }}
                                                });
            system.setArgument("gusty_function", new Dictionary<string, object> { { "name", "org.hpcshelf.gust.example.pgr.PGRANK" } });

            // 3.6. Specifying the contract of "reducer_1" and "reducer_2".
            IDictionary<string, object> contract_reducer = new Dictionary<string, object>()
            {
                {"name", "org.hpcshelf.gust.computation.Gusty"},
                {"platform_maintainer", "MPeer"},
                {"input_key_type", "TKey"},
                {"input_value_type", "TValue"},
                {"output_key_type", "TKey"},
                {"output_value_type", "TValue"},
                {"reduce_function", "RF"},
                {"graph_type", "G"},
                {"graph_input_format", "GIF"},
                {"reduce_function_action_port_type", "ATerminationPortType"}
            };

            // 3.7. Creating the objects that represent "reducer_1" and "reducer_2".
            IComputation reducer_1 = system.newComputation("reducer_1", contract_reducer, platform_reduce_1);
            IComputation reducer_2 = system.newComputation("reducer_2", contract_reducer, platform_reduce_2);

            // 4. CREATE THE "flatten_output" COMPONENT, that will be placed in "platform_SAFe".

            // 4.1. Specifying the contract of the "flatten_output" component.
            IDictionary<string, object> contract_flatten_output = new Dictionary<string, object>()
            {
                {"name", "org.hpcshelf.mapreduce.computation.Mapper"},
                {"platform_maintainer", "MPeer"},
                {"input_key_type", "TKey"},
                {"input_value_type", new Dictionary<string,object> {{"name","br.ufc.mdcc.common.Iterator"}, {"item_type","TValue"}}},
                {"output_key_type", "TKey"},
                {"output_value_type", "TValue"},
                {"map_function", new Dictionary<string, object> {{"name", "org.hpcshelf.mapreduce.custom.MapFunctionFlatten"}, {"key_type", "TKey"}, {"value_type","TValue"}/*, {"action_port_type", new Dictionary<string,object> {{"name", "br.ufc.mdcc.hpc.storm.binding.task.EmptyTaskPortType"}}}*/}}/*,
                {"map_function_action_port_type", new Dictionary<string,object> {{"name", "br.ufc.mdcc.hpc.storm.binding.task.EmptyTaskPortType"}}}*/
            };
             
            // 4.2. Creating the computation object that represent "flatten_output".
            IComputation flatten_output = system.newComputation("flatten_output", contract_flatten_output, system.PlatformSAFe);

            // 5. CREATING THE "shuffler" connector. 

            // 5.1. Adding context parameteres that will be referred in the contract of "shuffler".
            //system.addParameter("action_port_type_partition", "AA", new Dictionary<string, object> { { "name", "mapreduce.gust.iterative.port.task.TerminationFlagTaskPortType" } });
            system.addParameter("partition_function", "PF", new Dictionary<string, object> 
                                                                { 
                                                                   { "name", "org.hpcshelf.mapreduce.custom.PartitionFunctionIterative" },
                                                                   { "input_key", "TKey" }/*,
                                                                   { "action_port_type", new Dictionary<string,object> {{"name", "br.ufc.mdcc.hpc.storm.binding.task.EmptyTaskPortType"}}}*/
                                                                });

            // 5.2. Specifying the contract of "shuffler".
            IDictionary<string, object> contract_shuffler = new Dictionary<string, object>()
            {
                {"name", "org.hpcshelf.mapreduce.connector.Shuffler"},
                {"intermediate_key_type", "TKey"},
                {"intermediate_value_type", "TValue"},
                {"partition_function", "PF"}/*,
                {"partition_function_action_port_type", new Dictionary<string,object> {{"name", "br.ufc.mdcc.hpc.storm.binding.task.EmptyTaskPortType"}}}*/
            };

            // 5.2. Specifying where the facets of "shuffler" will be placed.
            Tuple<int, IVirtualPlatform>[] platform_shuffler = new Tuple<int, IVirtualPlatform>[] 
                                    { 
                                        new Tuple<int, IVirtualPlatform>(0, system.PlatformSAFe),    // local_output_pairs
                                        new Tuple<int, IVirtualPlatform>(0, platform_reduce_1),      // global_output_pairs_1
                                        new Tuple<int, IVirtualPlatform>(0, platform_reduce_2),      // global_output_pairs_2
                                        new Tuple<int, IVirtualPlatform>(1, platform_data_source),   // local_input_pairs (input_data)
                                        new Tuple<int, IVirtualPlatform>(1, platform_reduce_1),      // global_input_pairs_1
                                        new Tuple<int, IVirtualPlatform>(1, platform_reduce_2),      // global_input_pairs_2
                                    };

            // 5.2. Creating the connector object that represents "shuffler".
            IConnector shuffler = system.newConnector("shuffler", contract_shuffler, platform_shuffler);

            // 6. CREATING THE ACTION PORTS OF THE "workflow" system component.
            /* They are necessary for connecting the bindings between the "workflow" component and solution components */

            IActionPortWorkflow workflow_port_reduce_1               = system.Workflow.addActionPort("task_reduce_1","org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvance");
            IActionPortWorkflow workflow_port_reduce_2               = system.Workflow.addActionPort("task_reduce_2", "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvance");
            IActionPortWorkflow workflow_port_flatten_output         = system.Workflow.addActionPort("task_flatten_output", "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvance");
            IActionPortWorkflow workflow_port_active_status_0        = system.Workflow.addActionPort("task_shuffle_collector_active_status_0", "org.hpcshelf.mapreduce.port.task.TaskPortTypeActiveStatus");
            IActionPortWorkflow workflow_port_active_status_1        = system.Workflow.addActionPort("task_shuffle_collector_active_status_1", "org.hpcshelf.mapreduce.port.task.TaskPortTypeActiveStatus");
            IActionPortWorkflow workflow_port_active_status_2        = system.Workflow.addActionPort("task_shuffle_collector_active_status_2", "org.hpcshelf.mapreduce.port.task.TaskPortTypeActiveStatus");
            IActionPortWorkflow workflow_port_collector_read_chunk_0 = system.Workflow.addActionPort("task_shuffle_collector_read_chunk_0", "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvanceReadChunk");
            IActionPortWorkflow workflow_port_collector_read_chunk_1 = system.Workflow.addActionPort("task_shuffle_collector_read_chunk_1", "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvanceReadChunk");
            IActionPortWorkflow workflow_port_collector_read_chunk_2 = system.Workflow.addActionPort("task_shuffle_collector_read_chunk_2", "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvanceReadChunk");
            IActionPortWorkflow workflow_port_feeder_read_chunk_0    = system.Workflow.addActionPort("task_shuffle_feeder_read_chunk_0", "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvanceReadChunk");
            IActionPortWorkflow workflow_port_feeder_read_chunk_1    = system.Workflow.addActionPort("task_shuffle_feeder_read_chunk_1", "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvanceReadChunk");
            IActionPortWorkflow workflow_port_feeder_read_chunk_2    = system.Workflow.addActionPort("task_shuffle_feeder_read_chunk_2", "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvanceReadChunk");
            IActionPortWorkflow workflow_port_feeder_chunk_ready_0   = system.Workflow.addActionPort("task_shuffle_feeder_chunk_ready_0", "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvanceChunkReady");
            IActionPortWorkflow workflow_port_feeder_chunk_ready_1   = system.Workflow.addActionPort("task_shuffle_feeder_chunk_ready_1", "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvanceChunkReady");
            IActionPortWorkflow workflow_port_feeder_chunk_ready_2   = system.Workflow.addActionPort("task_shuffle_feeder_chunk_ready_2", "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvanceChunkReady");
            IActionPortWorkflow workflow_port_reduce_function_1      = system.Workflow.addActionPort("reduce_function_action_port_1", "ATerminationPortType");
            IActionPortWorkflow workflow_port_reduce_function_2      = system.Workflow.addActionPort("reduce_function_action_port_2", "ATerminationPortType");
//            IActionPortWorkflow workflow_port_check_termination_1    = system.Workflow.addActionPort("check_termination_action_port_1", "br.ufc.mdcc.hpc.storm.binding.task.EmptyTaskPortType");
//            IActionPortWorkflow workflow_port_check_termination_2    = system.Workflow.addActionPort("check_termination_action_port_2", "br.ufc.mdcc.hpc.storm.binding.task.EmptyTaskPortType");

            // 7. CREATING THE ACTION BINDINGS
            /* The arguments define the type of the action binding, which determines the supported action names, and a set of action ports 
             * of the solution components that will synchronize among them. Notice that each port is defined by an array, because, once each port 
             * belongs to a single facet they may be replicated when a facet of a connector is replicated accross a set of virtual platforms, 
             * such as "shuffler". For instance, see "platform_shuffler", above, where shuffler facets 0 (feede) and 1 (collect) have multiplicity 3.
             */

            IActionBinding task_reduce_1                          = system.newActionBinding("task_reduce_1", "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvance", new IActionPort[] { system.Workflow.ActionPort["task_reduce_1"][0], reducer_1.ActionPort["task_reduce"][0] });
            IActionBinding task_reduce_2                          = system.newActionBinding("task_reduce_2", "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvance", new IActionPort[] { system.Workflow.ActionPort["task_reduce_2"][0], reducer_2.ActionPort["task_reduce"][0] });
            IActionBinding task_flatten_output                    = system.newActionBinding("task_flatten_output", "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvance", new IActionPort[] { system.Workflow.ActionPort["task_flatten_output"][0], flatten_output.ActionPort["task_map"][0] }); 
            IActionBinding task_shuffle_collector_active_status_0 = system.newActionBinding("task_shuffle_collector_active_status_0", "org.hpcshelf.mapreduce.port.task.TaskPortTypeActiveStatus", new IActionPort[] { system.Workflow.ActionPort["task_shuffle_collector_active_status_0"][0], shuffler.ActionPort["task_shuffle_collector_active_status"][0] });
            IActionBinding task_shuffle_collector_active_status_1 = system.newActionBinding("task_shuffle_collector_active_status_1", "org.hpcshelf.mapreduce.port.task.TaskPortTypeActiveStatus", new IActionPort[] { system.Workflow.ActionPort["task_shuffle_collector_active_status_1"][0], shuffler.ActionPort["task_shuffle_collector_active_status"][1] });
            IActionBinding task_shuffle_collector_active_status_2 = system.newActionBinding("task_shuffle_collector_active_status_2", "org.hpcshelf.mapreduce.port.task.TaskPortTypeActiveStatus", new IActionPort[] { system.Workflow.ActionPort["task_shuffle_collector_active_status_2"][0], shuffler.ActionPort["task_shuffle_collector_active_status"][2] });
            IActionBinding task_shuffle_collector_read_chunk_0    = system.newActionBinding("task_shuffle_collector_read_chunk_0", "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvanceReadChunk", new IActionPort[] { system.Workflow.ActionPort["task_shuffle_collector_read_chunk_0"][0], shuffler.ActionPort["task_shuffle_collector_read_chunk"][0] });
            IActionBinding task_shuffle_collector_read_chunk_1    = system.newActionBinding("task_shuffle_collector_read_chunk_1", "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvanceReadChunk", new IActionPort[] { system.Workflow.ActionPort["task_shuffle_collector_read_chunk_1"][0], shuffler.ActionPort["task_shuffle_collector_read_chunk"][1] });
            IActionBinding task_shuffle_collector_read_chunk_2    = system.newActionBinding("task_shuffle_collector_read_chunk_2", "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvanceReadChunk", new IActionPort[] { system.Workflow.ActionPort["task_shuffle_collector_read_chunk_2"][0], shuffler.ActionPort["task_shuffle_collector_read_chunk"][2] });
            IActionBinding task_shuffle_feeder_read_chunk_0       = system.newActionBinding("task_shuffle_feeder_read_chunk_0", "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvanceReadChunk", new IActionPort[] { system.Workflow.ActionPort["task_shuffle_feeder_read_chunk_0"][0], shuffler.ActionPort["task_shuffle_feeder_read_chunk"][0] });
            IActionBinding task_shuffle_feeder_read_chunk_1       = system.newActionBinding("task_shuffle_feeder_read_chunk_1", "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvanceReadChunk", new IActionPort[] { system.Workflow.ActionPort["task_shuffle_feeder_read_chunk_1"][0], shuffler.ActionPort["task_shuffle_feeder_read_chunk"][1] });
            IActionBinding task_shuffle_feeder_read_chunk_2       = system.newActionBinding("task_shuffle_feeder_read_chunk_2", "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvanceReadChunk", new IActionPort[] { system.Workflow.ActionPort["task_shuffle_feeder_read_chunk_2"][0], shuffler.ActionPort["task_shuffle_feeder_read_chunk"][2] });
            IActionBinding task_shuffle_feeder_chunk_ready_0      = system.newActionBinding("task_shuffle_feeder_chunk_ready_0", "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvanceChunkReady", new IActionPort[] { system.Workflow.ActionPort["task_shuffle_feeder_chunk_ready_0"][0], shuffler.ActionPort["task_shuffle_feeder_chunk_ready"][0] });
            IActionBinding task_shuffle_feeder_chunk_ready_1      = system.newActionBinding("task_shuffle_feeder_chunk_ready_1", "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvanceChunkReady", new IActionPort[] { system.Workflow.ActionPort["task_shuffle_feeder_chunk_ready_1"][0], shuffler.ActionPort["task_shuffle_feeder_chunk_ready"][1] });
            IActionBinding task_shuffle_feeder_chunk_ready_2      = system.newActionBinding("task_shuffle_feeder_chunk_ready_2", "org.hpcshelf.mapreduce.port.task.TaskPortTypeAdvanceChunkReady", new IActionPort[] { system.Workflow.ActionPort["task_shuffle_feeder_chunk_ready_2"][0], shuffler.ActionPort["task_shuffle_feeder_chunk_ready"][2] });
            IActionBinding reduce_function_action_port_1          = system.newActionBinding("reduce_function_action_port_1", "ATerminationPortType", new IActionPort[] { system.Workflow.ActionPort["reduce_function_action_port_1"][0], reducer_1.ActionPort["reduce_function_action_port"][0] });
            IActionBinding reduce_function_action_port_2          = system.newActionBinding("reduce_function_action_port_2", "ATerminationPortType", new IActionPort[] { system.Workflow.ActionPort["reduce_function_action_port_2"][0], reducer_2.ActionPort["reduce_function_action_port"][0] });
       //     IActionBinding check_termination_action_port_1        = system.newActionBinding("check_termination_action_port_1", "br.ufc.mdcc.hpc.storm.binding.task.EmptyTaskPortType", new IActionPort[] { system.Workflow.ActionPort["check_termination_action_port_1"][0], shuffler.ActionPort["partition_function_action_port"][1] });
      //      IActionBinding check_termination_action_port_2        = system.newActionBinding("check_termination_action_port_2", "br.ufc.mdcc.hpc.storm.binding.task.EmptyTaskPortType", new IActionPort[] { system.Workflow.ActionPort["check_termination_action_port_2"][0], shuffler.ActionPort["partition_function_action_port"][2] });

            // 7. CREATING THE SERVICE BINDINGS

            // 7.1. Specifying the binding contracts.
            IDictionary<string, object> contract_service_input = new Dictionary<string, object>()
            {
                {"name", "br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentBindingBaseDirect"},
                {"client_port_type", new Dictionary<string,object>() {{"name","org.hpcshelf.mapreduce.port.environment.PortTypeIterator"}}},
                {"server_port_type", new Dictionary<string,object>() {{"name","org.hpcshelf.gust.port.environment.PortTypeDataSourceGraphInterface"}}}
            };

            IDictionary<string, object> contract_service_output = new Dictionary<string, object>()
            {
                {"name", "br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentBindingBaseDirect"},
                {"client_port_type", new Dictionary<string,object>() {{"name","org.hpcshelf.gust.port.environment.PortTypeDataSinkGraphInterface"}}},
                {"server_port_type", new Dictionary<string,object>() {{"name","org.hpcshelf.mapreduce.port.environment.PortTypeIterator"}}}
            };

            IDictionary<string, object> contract_service_pairs = new Dictionary<string, object>()
            {
                {"name", "br.ufc.mdcc.hpc.storm.binding.environment.EnvironmentBindingBaseDirect"},
                {"client_port_type", new Dictionary<string,object>() {{"name","org.hpcshelf.mapreduce.port.environment.PortTypeIterator"}}},
                {"server_port_type", new Dictionary<string,object>() {{"name","org.hpcshelf.mapreduce.port.environment.PortTypeIterator"}}}
            };

            // 7.2. Crating the objects that represent the bindings.
            IServiceBinding local_input_pairs = system.newServiceBinding("local_input_pairs", contract_service_input, shuffler.UserPort["collect_pairs"][0], source.ProviderPort["input_data"][0]);
            IServiceBinding local_output_pairs = system.newServiceBinding("local_output_pairs", contract_service_pairs, flatten_output.UserPort["collect_pairs"][0], shuffler.ProviderPort["feed_pairs"][0]); ;
            IServiceBinding global_input_pairs_1 = system.newServiceBinding("global_input_pairs_1", contract_service_pairs, reducer_1.UserPort["collect_pairs"][0], shuffler.ProviderPort["feed_pairs"][1]); ;
            IServiceBinding global_input_pairs_2 = system.newServiceBinding("global_input_pairs_2", contract_service_pairs, reducer_2.UserPort["collect_pairs"][0], shuffler.ProviderPort["feed_pairs"][2]); 
            IServiceBinding global_output_pairs_1 = system.newServiceBinding("global_output_pairs_1", contract_service_pairs, shuffler.UserPort["collect_pairs"][1], reducer_1.ProviderPort["feed_pairs"][0]); 
            IServiceBinding global_output_pairs_2 = system.newServiceBinding("global_output_pairs_2", contract_service_pairs, shuffler.UserPort["collect_pairs"][2], reducer_2.ProviderPort["feed_pairs"][0]);

            // 7.3. Adding a user port the the "application" system component, which will read the output.
            system.Application.addDirectUserPort("output_data", contract_service_output);

            // 7.4. Creating a binding to connect the "output_data" port of "application" to the "feed_pairs" port of "flatten_output".
            IServiceBinding output_data = system.newServiceBinding("output_data", contract_service_output, system.Application.UserPort["output_data"][0], flatten_output.ProviderPort["feed_pairs"][0]);

            // 8. CREATING THE OBJECTS REPRESENTING LIFE CYCLE CONTROL PORTS FOR EACH SOLUTION COMPONENT.
            /* TODO: Make them implicit ! */

            IActionPortLifeCycle source_lifecycle = system.Workflow.ActionPortLifeCycle["source"];
            IActionPortLifeCycle reducer_1_lifecycle = system.Workflow.ActionPortLifeCycle["reducer_1"];
            IActionPortLifeCycle reducer_2_lifecycle = system.Workflow.ActionPortLifeCycle["reducer_2"];
            IActionPortLifeCycle flatten_output_lifecycle = system.Workflow.ActionPortLifeCycle["flatten_output"];
            IActionPortLifeCycle shuffler_lifecycle = system.Workflow.ActionPortLifeCycle["shuffler"];
            IActionPortLifeCycle platform_source_lifecycle = system.Workflow.ActionPortLifeCycle["platform_data_source"];
            IActionPortLifeCycle platform_reducer_1_lifecycle = system.Workflow.ActionPortLifeCycle["platform_reduce_1"];
            IActionPortLifeCycle platform_reducer_2_lifecycle = system.Workflow.ActionPortLifeCycle["platform_reduce_2"];
            IActionPortLifeCycle local_input_pairs_lifecycle = system.Workflow.ActionPortLifeCycle["local_input_pairs"];
            IActionPortLifeCycle local_output_pairs_lifecycle = system.Workflow.ActionPortLifeCycle["local_output_pairs"];
            IActionPortLifeCycle global_input_pairs_1_lifecycle = system.Workflow.ActionPortLifeCycle["global_input_pairs_1"];
            IActionPortLifeCycle global_input_pairs_2_lifecycle = system.Workflow.ActionPortLifeCycle["global_input_pairs_2"];
            IActionPortLifeCycle global_output_pairs_1_lifecycle = system.Workflow.ActionPortLifeCycle["global_output_pairs_1"];
            IActionPortLifeCycle global_output_pairs_2_lifecycle = system.Workflow.ActionPortLifeCycle["global_output_pairs_2"];
            IActionPortLifeCycle output_data_lifecycle = system.Workflow.ActionPortLifeCycle["output_data"];
            IActionPortLifeCycle task_reduce_1_lifecycle = system.Workflow.ActionPortLifeCycle["task_reduce_1"];
            IActionPortLifeCycle task_reduce_2_lifecycle = system.Workflow.ActionPortLifeCycle["task_reduce_2"];
            IActionPortLifeCycle task_flatten_output_lifecycle = system.Workflow.ActionPortLifeCycle["task_flatten_output"];
            IActionPortLifeCycle task_shuffle_collector_active_status_0_lifecycle = system.Workflow.ActionPortLifeCycle["task_shuffle_collector_active_status_0"];
            IActionPortLifeCycle task_shuffle_collector_active_status_1_lifecycle = system.Workflow.ActionPortLifeCycle["task_shuffle_collector_active_status_1"];
            IActionPortLifeCycle task_shuffle_collector_active_status_2_lifecycle = system.Workflow.ActionPortLifeCycle["task_shuffle_collector_active_status_2"];
            IActionPortLifeCycle task_shuffle_collector_read_chunk_0_lifecycle = system.Workflow.ActionPortLifeCycle["task_shuffle_collector_read_chunk_0"];
            IActionPortLifeCycle task_shuffle_collector_read_chunk_1_lifecycle = system.Workflow.ActionPortLifeCycle["task_shuffle_collector_read_chunk_1"];
            IActionPortLifeCycle task_shuffle_collector_read_chunk_2_lifecycle = system.Workflow.ActionPortLifeCycle["task_shuffle_collector_read_chunk_2"];
            IActionPortLifeCycle task_shuffle_feeder_read_chunk_0_lifecycle = system.Workflow.ActionPortLifeCycle["task_shuffle_feeder_read_chunk_0"];
            IActionPortLifeCycle task_shuffle_feeder_read_chunk_1_lifecycle = system.Workflow.ActionPortLifeCycle["task_shuffle_feeder_read_chunk_1"];
            IActionPortLifeCycle task_shuffle_feeder_read_chunk_2_lifecycle = system.Workflow.ActionPortLifeCycle["task_shuffle_feeder_read_chunk_2"];
            IActionPortLifeCycle task_shuffle_feeder_chunk_ready_0_lifecycle = system.Workflow.ActionPortLifeCycle["task_shuffle_feeder_chunk_ready_0"];
            IActionPortLifeCycle task_shuffle_feeder_chunk_ready_1_lifecycle = system.Workflow.ActionPortLifeCycle["task_shuffle_feeder_chunk_ready_1"];
            IActionPortLifeCycle task_shuffle_feeder_chunk_ready_2_lifecycle = system.Workflow.ActionPortLifeCycle["task_shuffle_feeder_chunk_ready_2"];
            IActionPortLifeCycle reduce_function_action_port_1_lifecycle = system.Workflow.ActionPortLifeCycle["reduce_function_action_port_1"];
            IActionPortLifeCycle reduce_function_action_port_2_lifecycle = system.Workflow.ActionPortLifeCycle["reduce_function_action_port_2"];
			//      IActionPortLifeCycle check_termination_action_port_1_lifecycle = system.Workflow.ActionPortLifeCycle["check_termination_action_port_1"];
			//       IActionPortLifeCycle check_termination_action_port_2_lifecycle = system.Workflow.ActionPortLifeCycle["check_termination_action_port_2"];

			// 8. INITIATING THE ORCHESTRATION

			// 8.1. Firstly, creating all solution components.

			flatten_output_lifecycle.resolve();
			flatten_output_lifecycle.deploy();
			flatten_output_lifecycle.instantiate();
			
            platform_reducer_1_lifecycle.resolve();
            platform_reducer_1_lifecycle.deploy();
            platform_reducer_1_lifecycle.instantiate();

			reducer_1_lifecycle.resolve();
			reducer_1_lifecycle.deploy();
			reducer_1_lifecycle.instantiate();

			platform_reducer_2_lifecycle.resolve();
            platform_reducer_2_lifecycle.deploy();
            platform_reducer_2_lifecycle.instantiate();

			reducer_2_lifecycle.resolve();
			reducer_2_lifecycle.deploy();
			reducer_2_lifecycle.instantiate();

			platform_source_lifecycle.resolve();
			platform_source_lifecycle.deploy();
			platform_source_lifecycle.instantiate();

			source_lifecycle.resolve();
			source_lifecycle.deploy();
			source_lifecycle.instantiate();

			shuffler_lifecycle.resolve();
            shuffler_lifecycle.deploy();
            shuffler_lifecycle.instantiate();
 
            local_input_pairs_lifecycle.resolve();
            local_input_pairs_lifecycle.deploy();
            local_input_pairs_lifecycle.instantiate();

            local_output_pairs_lifecycle.resolve();
            local_output_pairs_lifecycle.deploy();
            local_output_pairs_lifecycle.instantiate();

            global_input_pairs_1_lifecycle.resolve();
            global_input_pairs_1_lifecycle.deploy();
            global_input_pairs_1_lifecycle.instantiate();

            global_input_pairs_2_lifecycle.resolve();
            global_input_pairs_2_lifecycle.deploy();
            global_input_pairs_2_lifecycle.instantiate();

            global_output_pairs_1_lifecycle.resolve();
            global_output_pairs_1_lifecycle.deploy();
            global_output_pairs_1_lifecycle.instantiate();

            global_output_pairs_2_lifecycle.resolve();
            global_output_pairs_2_lifecycle.deploy();
            global_output_pairs_2_lifecycle.instantiate();

            task_reduce_1_lifecycle.resolve();
            task_reduce_1_lifecycle.deploy();
            task_reduce_1_lifecycle.instantiate();

            task_reduce_2_lifecycle.resolve();
            task_reduce_2_lifecycle.deploy();
            task_reduce_2_lifecycle.instantiate();

            task_shuffle_collector_active_status_0_lifecycle.resolve();
            task_shuffle_collector_active_status_0_lifecycle.deploy();
            task_shuffle_collector_active_status_0_lifecycle.instantiate();

            task_shuffle_collector_active_status_1_lifecycle.resolve();
            task_shuffle_collector_active_status_1_lifecycle.deploy();
            task_shuffle_collector_active_status_1_lifecycle.instantiate();

            task_shuffle_collector_active_status_2_lifecycle.resolve();
            task_shuffle_collector_active_status_2_lifecycle.deploy();
            task_shuffle_collector_active_status_2_lifecycle.instantiate();

            task_shuffle_collector_read_chunk_0_lifecycle.resolve();
            task_shuffle_collector_read_chunk_0_lifecycle.deploy();
            task_shuffle_collector_read_chunk_0_lifecycle.instantiate();

            task_shuffle_collector_read_chunk_1_lifecycle.resolve();
            task_shuffle_collector_read_chunk_1_lifecycle.deploy();
            task_shuffle_collector_read_chunk_1_lifecycle.instantiate();

            task_shuffle_collector_read_chunk_2_lifecycle.resolve();
            task_shuffle_collector_read_chunk_2_lifecycle.deploy();
            task_shuffle_collector_read_chunk_2_lifecycle.instantiate();
            
            task_shuffle_feeder_read_chunk_0_lifecycle.resolve();
            task_shuffle_feeder_read_chunk_0_lifecycle.deploy();
            task_shuffle_feeder_read_chunk_0_lifecycle.instantiate();

            task_shuffle_feeder_read_chunk_1_lifecycle.resolve();
            task_shuffle_feeder_read_chunk_1_lifecycle.deploy();
            task_shuffle_feeder_read_chunk_1_lifecycle.instantiate();

            task_shuffle_feeder_read_chunk_2_lifecycle.resolve();
            task_shuffle_feeder_read_chunk_2_lifecycle.deploy();
            task_shuffle_feeder_read_chunk_2_lifecycle.instantiate();

            task_shuffle_feeder_chunk_ready_1_lifecycle.resolve();
            task_shuffle_feeder_chunk_ready_1_lifecycle.deploy();
            task_shuffle_feeder_chunk_ready_1_lifecycle.instantiate();

            task_shuffle_feeder_chunk_ready_2_lifecycle.resolve();
            task_shuffle_feeder_chunk_ready_2_lifecycle.deploy();
            task_shuffle_feeder_chunk_ready_2_lifecycle.instantiate();

            reduce_function_action_port_1_lifecycle.resolve();
            reduce_function_action_port_1_lifecycle.deploy();
            reduce_function_action_port_1_lifecycle.instantiate();

            reduce_function_action_port_2_lifecycle.resolve();
            reduce_function_action_port_2_lifecycle.deploy();
            reduce_function_action_port_2_lifecycle.instantiate();

        //    check_termination_action_port_1_lifecycle.resolve();
       //     check_termination_action_port_1_lifecycle.deploy();
         //   check_termination_action_port_1_lifecycle.instantiate();

          //  check_termination_action_port_2_lifecycle.resolve();
       //     check_termination_action_port_2_lifecycle.deploy();
        //    check_termination_action_port_2_lifecycle.instantiate();

            source_lifecycle.run();
            reducer_1_lifecycle.run();
            reducer_2_lifecycle.run();
            shuffler_lifecycle.run();

            // 8.2. This is a thread for reiveing the output.
            Thread thread_receive_output = new Thread(() =>
            {
                Console.WriteLine("thread_receive_output 1");

                IClientBase<IPortTypeDataSinkGraphInterface> output_data_port = (IClientBase<IPortTypeDataSinkGraphInterface>)system.Application.Services.getPort("output_data");

                Console.WriteLine("thread_receive_output 2");

               // IOutputFormat<IVertexBasic, IDataPGRANK> graph_output_format = (IOutputFormat<IVertexBasic, IDataPGRANK>)system.Application.Services.getPort("output_format");

                Console.WriteLine("thread_receive_output 3");

                long t0 = (long)(DateTime.UtcNow - (new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))).TotalMilliseconds;
                //IOutputFormatInstance<IVertexBasic, IDataPGRANK> gof = (IOutputFormatInstance<IVertexBasic, IDataPGRANK>)graph_output_format.Instance;
                Console.WriteLine("IDataSinkGraph: START 1 output_file_path = {0}", OUTPUT_FILE_PATH);
                Console.WriteLine("output_data is null ? {0}", output_data_port == null);
                IPortTypeDataSinkGraphInterface pair_reader = output_data_port.Client;
                Console.WriteLine("pair_reader is null ? {0}", pair_reader == null);
                ConcurrentQueue<Tuple<object, int>> pairs = pair_reader.readPairs();
                Semaphore not_empty = pair_reader.NotEmpty;
                Console.WriteLine("IDataSinkGraph: BEFORE LOOP");
                Tuple<object, int> pair;

                int sum = 0;

                do
                {
                    Console.WriteLine("IDataSinkGraph: LOOP -- BEFORE not_empty LOCK");
                    not_empty.WaitOne();
                    Console.WriteLine("IDataSinkGraph: LOOP -- AFTER not_empty LOCK");
                    pairs.TryDequeue(out pair);
                    if (pair.Item2 > 0)
                        /*gof.*/sendToFile(OUTPUT_FILE_PATH, pair.Item1);
                    sum += pair.Item2;
                } while (pair.Item2 > 0);

                Console.WriteLine("IDataSink: FINISH sum={0}", sum);
                long t1 = (long)(DateTime.UtcNow - (new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc))).TotalMilliseconds;
                Console.WriteLine("Time processing: " + (t1 - t0) + " ms");
                File.AppendAllText(OUTPUT_FILE_PATH + ".time", (t1 - t0) + "" + System.Environment.NewLine);

            });

            thread_receive_output.Start();

            // 8.3. TASK ORCHESTRATION ...

            Thread t_active_status_1_inactive = new Thread(() =>
            {
                workflow_port_active_status_1.invoke("CHANGE_STATUS_BEGIN");
                workflow_port_active_status_1.invoke("INACTIVE");
                workflow_port_active_status_1.invoke("CHANGE_STATUS_END");
            });

            Thread t_active_status_2_inactive = new Thread(() =>
            {
                workflow_port_active_status_2.invoke("CHANGE_STATUS_BEGIN");
                workflow_port_active_status_2.invoke("INACTIVE");
                workflow_port_active_status_2.invoke("CHANGE_STATUS_END");
            });

            t_active_status_1_inactive.Start();
            t_active_status_2_inactive.Start();
            t_active_status_1_inactive.Join();
            t_active_status_2_inactive.Join();

            while (workflow_port_collector_read_chunk_0.invoke(new string[] { "READ_CHUNK", "FINISH_CHUNK" }).Equals("READ_CHUNK"))
            {
                Thread t_reduce_1_first_iteration = new Thread(() =>
                {
                    workflow_port_feeder_read_chunk_1.invoke("READ_CHUNK");
                    workflow_port_feeder_chunk_ready_1.invoke("CHUNK_READY");
                    workflow_port_reduce_1.invoke("READ_CHUNK");
                    workflow_port_reduce_1.invoke("PERFORM");
                });

                Thread t_reduce_2_first_iteration = new Thread(() =>
                {
                    workflow_port_feeder_read_chunk_2.invoke("READ_CHUNK");
                    workflow_port_feeder_chunk_ready_2.invoke("CHUNK_READY");
                    workflow_port_reduce_2.invoke("READ_CHUNK");
                    workflow_port_reduce_2.invoke("PERFORM");
                });

                t_reduce_1_first_iteration.Start();
                t_reduce_2_first_iteration.Start();

                t_reduce_1_first_iteration.Join();
                t_reduce_2_first_iteration.Join();
            }

            IActionFuture f1, f2, f3, f4;

            workflow_port_feeder_read_chunk_1.invoke("FINISH_CHUNK", out f1);
            workflow_port_feeder_read_chunk_2.invoke("FINISH_CHUNK", out f2);
            workflow_port_reduce_1.invoke("FINISH_CHUNK", out f3);
            workflow_port_reduce_2.invoke("FINISH_CHUNK", out f4);
            IActionFutureSet fs = f1.createSet();
            fs.addAction(f2); fs.addAction(f3); fs.addAction(f4); 
            fs.waitAll();

            workflow_port_reduce_1.invoke("CHUNK_READY", out f1);
            workflow_port_reduce_2.invoke("CHUNK_READY", out f2);
            fs = f1.createSet();
            fs.addAction(f1); fs.addAction(f2);
            fs.waitAll();

            Thread t_active_status_0_inactive = new Thread(() =>
            {
                workflow_port_active_status_0.invoke("CHANGE_STATUS_BEGIN");
                workflow_port_active_status_0.invoke("INACTIVE");
                workflow_port_active_status_0.invoke("CHANGE_STATUS_END");
            });

            Thread t_active_status_1_active = new Thread(() =>
            {
                workflow_port_active_status_1.invoke("CHANGE_STATUS_BEGIN");
                workflow_port_active_status_1.invoke("ACTIVE");
                workflow_port_active_status_1.invoke("CHANGE_STATUS_END");
            });

            Thread t_active_status_2_active = new Thread(() =>
            {
                workflow_port_active_status_2.invoke("CHANGE_STATUS_BEGIN");
                workflow_port_active_status_2.invoke("ACTIVE");
                workflow_port_active_status_2.invoke("CHANGE_STATUS_END");
            });

            t_active_status_0_inactive.Start();
            t_active_status_1_active.Start();
            t_active_status_2_active.Start();

            t_active_status_0_inactive.Join();
            t_active_status_1_active.Join();
            t_active_status_2_active.Join();

            Semaphore wait_0 = new Semaphore(0, int.MaxValue);
            Semaphore wait_1 = new Semaphore(0, int.MaxValue);

            Thread t_main_loop = new Thread(() =>
                {

                    bool main_loop_termination = false;
                    while (!main_loop_termination)
                    {
                        string main_loop_condition = workflow_port_reduce_function_1.invoke(new string[] { "CONTINUE", "TERMINATE" }) as string;
                        switch (main_loop_condition)
                        {
                            case "CONTINUE":
                                workflow_port_reduce_function_2.invoke("CONTINUE");
                                bool next_iteration_termination = false;
                                while (!next_iteration_termination)
                                {
                                    string next_iteration_condition = workflow_port_collector_read_chunk_1.invoke(new string[] { "READ_CHUNK", "FINISH_CHUNK" }) as string;
                                    switch (next_iteration_condition)
                                    {
                                        case "READ_CHUNK":
                                            workflow_port_collector_read_chunk_2.invoke("READ_CHUNK");
                                            Thread t_reduce_1_next_iteration = new Thread(() =>
                                            {
                                                workflow_port_feeder_read_chunk_1.invoke("READ_CHUNK");
                                                workflow_port_feeder_chunk_ready_1.invoke("CHUNK_READY");
                                                workflow_port_reduce_1.invoke("READ_CHUNK");
                                                workflow_port_reduce_1.invoke("PERFORM");
                                            });

                                            Thread t_reduce_2_next_iteration = new Thread(() =>
                                            {
                                                workflow_port_feeder_read_chunk_2.invoke("READ_CHUNK");
                                                workflow_port_feeder_chunk_ready_2.invoke("CHUNK_READY");
                                                workflow_port_reduce_2.invoke("READ_CHUNK");
                                                workflow_port_reduce_2.invoke("PERFORM");
                                            });

                                            t_reduce_1_next_iteration.Start();
                                            t_reduce_2_next_iteration.Start();

                                            t_reduce_1_next_iteration.Join();
                                            t_reduce_2_next_iteration.Join();
                                            break;
                                        case "FINISH_CHUNK":
                                            workflow_port_collector_read_chunk_2.invoke("FINISH_CHUNK");
                                            next_iteration_termination = true;
                                            break;
                                    }
                                }
                                workflow_port_feeder_read_chunk_1.invoke("FINISH_CHUNK", out f1);
                                workflow_port_feeder_read_chunk_2.invoke("FINISH_CHUNK", out f2);
                                workflow_port_reduce_1.invoke("FINISH_CHUNK", out f3);
                                workflow_port_reduce_2.invoke("FINISH_CHUNK", out f4);
                                fs = f1.createSet();
                                fs.addAction(f2); fs.addAction(f3); fs.addAction(f4);
                                fs.waitAll();

                                workflow_port_reduce_1.invoke("CHUNK_READY", out f1);
                                workflow_port_reduce_2.invoke("CHUNK_READY", out f2);
                                fs = f1.createSet();
                                fs.addAction(f1); fs.addAction(f2);
                                fs.waitAll();
                                break;

                            case "TERMINATE":
                                Thread t_complete_shuffle_feeder_0 = new Thread(() =>
                                {
                                    wait_0.WaitOne();
                                    workflow_port_feeder_read_chunk_0.invoke("READ_CHUNK");
                                    workflow_port_feeder_chunk_ready_0.invoke("CHUNK_READY");
                                    workflow_port_feeder_read_chunk_0.invoke("FINISH_CHUNK");
                                });

                                Thread t_complete_flatten_output = new Thread(() =>
                                {
                                    wait_1.WaitOne();
                                    workflow_port_flatten_output.invoke("READ_CHUNK");
                                    workflow_port_flatten_output.invoke("PERFORM");
                                    workflow_port_flatten_output.invoke("CHUNK_READY");
                                    workflow_port_flatten_output.invoke("FINISH_CHUNK");
                                });

                                workflow_port_reduce_function_2.invoke("TERMINATE", out f1);
                                workflow_port_collector_read_chunk_1.invoke("READ_CHUNK", out f2);
                                workflow_port_collector_read_chunk_2.invoke("READ_CHUNK", out f3);
                                fs = f1.createSet();
                                fs.addAction(f1); fs.addAction(f2);fs.addAction(f3);

                                t_complete_shuffle_feeder_0.Start();
                                t_complete_flatten_output.Start();

                                t_complete_shuffle_feeder_0.Join();
                                t_complete_flatten_output.Join();

                                fs.waitAll();

                                main_loop_termination = true;
                                break;
                        }
                    }


                });

            Thread t_create_output_components = new Thread(() =>
                {
                    task_shuffle_feeder_chunk_ready_0_lifecycle.resolve();
                    task_shuffle_feeder_chunk_ready_0_lifecycle.deploy();
                    task_shuffle_feeder_chunk_ready_0_lifecycle.instantiate();

                    wait_0.Release();
                    output_data_lifecycle.resolve();
                    output_data_lifecycle.deploy();
                    output_data_lifecycle.instantiate();

                    task_flatten_output_lifecycle.resolve();
                    task_flatten_output_lifecycle.deploy();
                    task_flatten_output_lifecycle.instantiate();

                    wait_1.Release();
                    flatten_output_lifecycle.run();
                });

            t_main_loop.Start();
            t_create_output_components.Start();
            t_main_loop.Join();
            t_create_output_components.Join();

            thread_receive_output.Join();

            Console.WriteLine("...");
        }

        public static void sendToFile(string filename, object o)
        {
            Console.WriteLine("SEND TO FILE {0} --- TYPE: {1}", filename, o.GetType());
            IKVPairInstance<IVertexBasic, IDataPGRANK> kv = (IKVPairInstance<IVertexBasic, IDataPGRANK>)o;

            string s = kv.Key + " " + kv.Value;
            File.AppendAllText(filename, s + System.Environment.NewLine);
        }
    }
}
