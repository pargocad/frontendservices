﻿using System;
using System.Collections.Generic;
using org.hpcshelf.kinds;
using HPCShelfDriver;

namespace DriverTest
{
    class MainClass
    {
        // SET GLOBAL max_heap_table_size = 1024 * 1024 * 1024 * 4;
        // SET GLOBAL tmp_table_size = 1024 * 1024 * 1024 * 4;
        // exit
        // mysql -u HPCShelfCore -p hashmodel
        // source all.sql;
        // source update_parameters.sql;

        private static int N = int.Parse(System.Environment.GetEnvironmentVariable("N"));

        public static void Main(string[] args)
        {
            IDictionary<string, Tuple<string, object>> contract_signature = new Dictionary<string, Tuple<string, object>>();
            ISystem system = SystemBuilder.build("native_binding", contract_signature);

            IDictionary<string, object> contract_platform = new Dictionary<string, object>()
            {
                   {"name", "org.hpcshelf.platform.Platform"},
                   {"maintainer", new Dictionary<string, object> { { "name", "org.hpcshelf.platform.maintainer.LocalHost" } }},
                   {"node-count", 2}
            };

            IVirtualPlatform[] platform = new IVirtualPlatform[N];
            for (int i = 0; i < N; i++)
                platform[i] = system.newVirtualPlatform(String.Format("platform[{0}]", i), contract_platform);

            IDictionary<string, object>[] computation_ports =
                {
                  /* port_0 */  new Dictionary<string, object>() 
                                    { {"intercommunicator_port_type", new Dictionary<string,object>() {{"name", "org.hpcshelf.mpi.intercommunicator.BasicSendReceive" }}} },
                  /* port_1 */  new Dictionary<string, object>()
                                    { {"intercommunicator_port_type", new Dictionary<string,object>() {{"name", "org.hpcshelf.mpi.intercommunicator.BasicSendReceive" }}} }
                };


            system.createComputation("teste.MPIProgram37", new string[] { "mpirun" }, computation_ports);

            IDictionary<string, object> contract_computation = new Dictionary<string, object>()
            {
                {"name", "teste.MPIProgram37"},
                {"language", new Dictionary<string, object> { { "name", "org.hpcshelf.mpi.language.C" } } },
                {"platform-node-operating_system", new Dictionary<string, object> { { "name", "org.hpcshelf.platform.node.os.linux.Ubuntu" } }},
                {"platform-node-count", 4}
            };

            IComputation[] computation = new IComputation[N];
            for (int i = 0; i < N; i++)
                computation[i] = system.newComputation(string.Format("computation[{0}]", i), contract_computation, platform[i]);

            IDictionary<string, object> contract_connector = new Dictionary<string, object>()
            {
                {"name", "org.hpcshelf.mpi.MPIConnectorForC"}/*,
                {"language", new Dictionary<string, object> { { "name", "org.hpcshelf.mpi.language.C" } } }*/
            };

            Tuple<int, IVirtualPlatform>[] platform_connector = new Tuple<int, IVirtualPlatform>[N];
            for (int i = 0; i < N; i++)
                platform_connector[i] = new Tuple<int, IVirtualPlatform>(0, platform[i]);

            IConnector connector_forward = system.newConnector("connector_forward", contract_connector, platform_connector);
            IConnector connector_backward = system.newConnector("connector_backward", contract_connector, platform_connector);

            IDictionary<string, object> binding_contract = new Dictionary<string, object>()
            {
                {"name", "org.hpcshelf.mpi.binding.IntercommunicatorBinding"},
                {"intercommunicator_port_type", new Dictionary<string,object>() {{"name", "org.hpcshelf.mpi.intercommunicator.BasicSendReceive" } }} /*,
                {"server_port_type", new Dictionary<string,object>() {{"name", "org.hpcshelf.mpi.intercommunicator.BasicSendReceive" } }}*/
            };

            IServiceBinding[] binding_forward = new IServiceBinding[N];
            for (int i = 0; i < N; i++)
                binding_forward[i] = system.newServiceBinding(string.Format("binding_forward[{0}]", i), binding_contract, computation[i].UserPort["port_0"][0], connector_forward.ProviderPort["port"][i]);

            IServiceBinding[] binding_backward = new IServiceBinding[N];
            for (int i = 0; i < N; i++)
                binding_backward[i] = system.newServiceBinding(string.Format("binding_backward[{0}]", i), binding_contract, computation[i].UserPort["port_1"][0], connector_backward.ProviderPort["port"][i]);

            IActionPortLifeCycle[] computation_lifecycle = new IActionPortLifeCycle[N];
            IActionPortLifeCycle[] platform_lifecycle = new IActionPortLifeCycle[N];
            IActionPortLifeCycle connector_forward_lifecycle;
            IActionPortLifeCycle connector_backward_lifecycle;
            IActionPortLifeCycle[] binding_forward_lifecycle = new IActionPortLifeCycle[N];
            IActionPortLifeCycle[] binding_backward_lifecycle = new IActionPortLifeCycle[N];

            for (int i = 0; i < N; i++)
            {
                computation_lifecycle[i] = (IActionPortLifeCycle)system.Workflow.ActionPort[string.Format("computation[{0}].lifecycle", i)][0];
                platform_lifecycle[i] = (IActionPortLifeCycle)system.Workflow.ActionPort[string.Format("platform[{0}].lifecycle", i)][0];
                binding_forward_lifecycle[i] = (IActionPortLifeCycle)system.Workflow.ActionPort[string.Format("binding_forward[{0}].lifecycle", i)][0];
                binding_backward_lifecycle[i] = (IActionPortLifeCycle)system.Workflow.ActionPort[string.Format("binding_backward[{0}].lifecycle", i)][0];
            }

            connector_forward_lifecycle = (IActionPortLifeCycle)system.Workflow.ActionPort["connector_forward.lifecycle"][0];
            connector_backward_lifecycle = (IActionPortLifeCycle)system.Workflow.ActionPort["connector_backward.lifecycle"][0];

            for (int i = 0; i < N; i++)
            {
                platform_lifecycle[i].resolve();
                platform_lifecycle[i].deploy();
                platform_lifecycle[i].instantiate();

                computation_lifecycle[i].resolve();
                computation_lifecycle[i].deploy();
                computation_lifecycle[i].instantiate();
            }

            connector_forward_lifecycle.resolve();
            connector_forward_lifecycle.deploy();
            connector_forward_lifecycle.instantiate();

            connector_backward_lifecycle.resolve();
            connector_backward_lifecycle.deploy();
            connector_backward_lifecycle.instantiate();

            for (int i = 0; i < N; i++)
            {
                binding_forward_lifecycle[i].resolve();
                binding_forward_lifecycle[i].deploy();
                binding_forward_lifecycle[i].instantiate();
            }

            for (int i = 0; i < N; i++)
            {
                binding_backward_lifecycle[i].resolve();
                binding_backward_lifecycle[i].deploy();
                binding_backward_lifecycle[i].instantiate();
            }

            IActionFuture[] f_computation = new IActionFuture[N];
            IActionFuture f_connector_forward; connector_forward_lifecycle.run(out f_connector_forward);
            IActionFuture f_connector_backward; connector_backward_lifecycle.run(out f_connector_backward);
            for (int i = 0; i < N; i++)
                computation_lifecycle[i].run(out f_computation[i]);

            IActionFutureSet fs = f_connector_forward.createSet();
            fs.addAction(f_connector_backward);
            for (int i = 0; i < N; i++)
                fs.addAction(f_computation[i]);

            fs.waitAll();


            // WAIT FOR THE END OF RUN ... (use task port) ...


           /* for (int i = 0; i < N; i++)
                binding_lifecycle[i].release();
            connector_lifecycle.release();
            for (int i = 0; i < N; i++)
            {
                computation_lifecycle[i].release();
                platform_lifecycle[i].release();
            }*/
        }
    }
}
