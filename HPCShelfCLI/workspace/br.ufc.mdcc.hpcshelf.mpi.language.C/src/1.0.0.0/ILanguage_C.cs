using br.ufc.pargo.hpe.kinds;
using br.ufc.mdcc.hpcshelf.mpi.Language;

namespace br.ufc.mdcc.hpcshelf.mpi.language.C
{
	public interface ILanguage_C : BaseILanguage_C, ILanguage
	{
	}
}